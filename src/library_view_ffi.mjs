import { js_custom_event } from "./elekf/web/events/scroll_to.mjs";

export function requestScroll(pos) {
  const el = getEl();
  if (el) {
    const e = js_custom_event(pos);
    el.dispatchEvent(e);
  }
}

export function addScrollendListener(callback) {
  const el = getEl();
  if (el) {
    el.addEventListener(
      "scrollend",
      () => {
        callback(el.scrollTop);
      },
      { passive: true }
    );
  }
}

export function scrollTo(pos) {
  const el = getEl();
  if (el) {
    el.scrollTo({
      top: pos,
      behavior: "instant",
    });
  }
}

function getEl() {
  return document.getElementById("library-list");
}
