import { Some, None } from "../gleam_stdlib/gleam/option.mjs";
import { Finite } from "./plinth/browser/media/position.mjs";
import {
  HangUp,
  NextSlide,
  NextTrack,
  Pause,
  Play,
  PreviousSlide,
  PreviousTrack,
  SeekBackward,
  SeekForward,
  SeekTo,
  SkipAd,
  Stop,
  ToggleCamera,
  ToggleMicrophone,
  ActionData,
} from "./plinth/browser/media/action.mjs";

const ACTION_TO_STR = new Map([
  [HangUp, "hangup"],
  [NextSlide, "nextslide"],
  [NextTrack, "nexttrack"],
  [Pause, "pause"],
  [Play, "play"],
  [PreviousSlide, "previousslide"],
  [PreviousTrack, "previoustrack"],
  [SeekBackward, "seekbackward"],
  [SeekForward, "seekforward"],
  [SeekTo, "seekto"],
  [SkipAd, "skipad"],
  [Stop, "stop"],
  [ToggleCamera, "togglecamera"],
  [ToggleMicrophone, "togglemicrophone"],
]);

const STR_TO_ACTION = new Map();
for (const [k, v] of ACTION_TO_STR) {
  STR_TO_ACTION.set(v, k);
}

export function get() {
  return navigator.mediaSession.metadata ?? new globalThis.MediaMetadata();
}

export function set(metadata) {
  navigator.mediaSession.metadata = new globalThis.MediaMetadata({
    title: metadata.title,
    artist: metadata.artist,
    album: metadata.album,
    artwork: metadata.artwork.toArray().map((art) => artwork2Obj(art)),
  });
}

export function setPositionState(new_state) {
  // If duration doesn't exist, this must be a ClearState (can't import it
  // because it's opaque)
  if (new_state.duration === undefined) {
    navigator.mediaSession.setPositionState();
  } else {
    const state = {
      duration:
        new_state.duration instanceof Finite
          ? new_state.duration.seconds
          : Infinity,
    };

    if (new_state.playback_rate instanceof Some) {
      state.playbackRate = new_state.playback_rate[0];
    }

    if (new_state.position instanceof Some) {
      state.position = new_state.position[0];
    }

    navigator.mediaSession.setPositionState(state);
  }
}

export function setActionHandler(action, handler) {
  navigator.mediaSession.setActionHandler(
    ACTION_TO_STR.get(action.constructor),
    (details) => {
      const actionConstructor = STR_TO_ACTION.get(details.action);
      const action = new actionConstructor();
      const gleamDetails = new ActionData(
        action,
        details.fastSeek ? new Some(details.fastSeek) : new None(),
        details.seekOffset ? new Some(details.seekOffset) : new None(),
        details.seekTime ? new Some(details.seekTime) : new None()
      );

      handler(gleamDetails);
    }
  );
}

function artwork2Obj(artwork) {
  return {
    src: artwork.src,
    sizes: artwork.sizes,
    type: artwork.type,
  };
}
