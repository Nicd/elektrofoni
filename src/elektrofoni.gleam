import ibroadcast/app_info.{AppInfo}

/// Name of the application.
pub const app_name = "elektrofoni"

/// Version of the application.
pub const app_version = "1.0.0"

/// ID of the application.
pub const app_id = 1098

/// Constant app info to use in application.
pub const app_info = AppInfo(app_name, app_version)

/// The bitrate to use for streaming by default.
pub const bitrate = 256

/// The expiry of the MP3 URLs generated for streaming, from the current moment
/// onwards, in milliseconds.
pub const track_expiry_length = 10_800_000

/// From 0.0 to 1.0, how much of a song has to be played for it to be tracked as
/// a single "play" (which is recorded in Last.FM also).
pub const play_record_ratio = 0.5
