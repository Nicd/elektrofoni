import { Ok, Error } from "./gleam.mjs";

function construct(input) {
  const d = new Date(input);

  if (isNaN(d.valueOf())) {
    return new Error(undefined);
  } else {
    return new Ok(d);
  }
}

export const from_iso8601 = construct;
export const from_unix = construct;

export function to_iso8601(date) {
  return date.toISOString();
}

export function to_unix(date) {
  return date.getTime();
}

export function unix_now() {
  return Date.now();
}

export function now() {
  return new Date();
}

export function equals(a, b) {
  return a.getTime() === b.getTime();
}

export function bigger_than(a, b) {
  return a > b;
}
