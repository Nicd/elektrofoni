import { Ok, Error } from "./gleam.mjs";

export function newEvent(name, data) {
  return new CustomEvent(name, { detail: data });
}

export function getDetail(e) {
  if ("detail" in e) {
    return new Ok(e.detail);
  }

  return new Error(undefined);
}
