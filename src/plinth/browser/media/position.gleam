import gleam/option
import gleam/result

/// Total duration of the played media.
pub type Duration {
  /// The media has a finite duration, in seconds.
  Finite(seconds: Float)

  /// The media has no finite duration (e.g. a stream).
  Infinite
}

/// The position, duration, and playback rate of the current media. Only
/// duration is mandatory.
pub opaque type PositionState {
  PositionState(
    duration: Duration,
    playback_rate: option.Option(Float),
    position: option.Option(Float),
  )
  ClearState
}

/// Make a position state with the given information.
///
/// There are some rules with the data:
/// * The duration must be bigger than 0.
/// * The position, if given, must be between 0 and duration.
/// * The playback rate can't be 0, but it can be negative.
pub fn make_position_state(
  duration: Duration,
  playback_rate: option.Option(Float),
  position: option.Option(Float),
) -> Result(PositionState, Nil) {
  use playback_rate <- result.try(parse_playback_rate(playback_rate))
  use duration <- result.try(parse_duration(duration))
  use position <- result.map(parse_position(position, duration))

  PositionState(
    duration: duration,
    playback_rate: playback_rate,
    position: position,
  )
}

/// Make a position state that clears the current information when set.
pub fn make_clear_position_state() -> PositionState {
  ClearState
}

fn parse_position(position, duration) {
  case position, duration {
    option.None, _ -> Ok(position)
    option.Some(val), Infinite if val >=. 0.0 -> Ok(position)
    option.Some(val), Finite(dur) if val >=. 0.0 && val <=. dur -> Ok(position)
    _otherwise, _ -> Error(Nil)
  }
}

fn parse_duration(duration) {
  case duration {
    Infinite -> Ok(duration)
    Finite(dur) if dur >=. 0.0 -> Ok(duration)
    _otherwise -> Error(Nil)
  }
}

fn parse_playback_rate(playback_rate) {
  case playback_rate {
    option.Some(0.0) -> Error(Nil)
    _otherwise -> Ok(playback_rate)
  }
}
