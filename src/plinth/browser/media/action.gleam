import gleam/option

/// Action type that can have a callback registered for it.
pub type Action {
  /// End a call.
  HangUp

  /// Moves to the next slide, when presenting a slide deck.
  NextSlide

  /// Advances playback to the next track.
  NextTrack

  /// Pauses playback of the media.
  Pause

  /// Begins (or resumes) playback of the media.
  Play

  /// Moves to the previous slide, when presenting a slide deck.
  PreviousSlide

  /// Moves back to the previous track.
  PreviousTrack

  /// Seeks backward through the media from the current position.
  SeekBackward

  /// Seeks forward from the current position through the media.
  SeekForward

  /// Moves the playback position to the specified time within the media.
  SeekTo

  /// Skips past the currently playing advertisement or commercial.
  SkipAd

  /// Halts playback entirely.
  Stop

  /// Turn the user's active camera on or off.
  ToggleCamera

  /// Mute or unmute the user's microphone.
  ToggleMicrophone
}

/// Data sent to action handler callback.
pub type ActionData {
  ActionData(
    action: Action,
    fast_seek: option.Option(Bool),
    seek_offset: option.Option(Float),
    seek_time: option.Option(Float),
  )
}
