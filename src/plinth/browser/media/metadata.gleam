pub type MetadataArtwork {
  MetadataArtwork(src: String, sizes: String, type_: String)
}

pub opaque type MediaMetadata {
  MediaMetadata(
    title: String,
    artist: String,
    album: String,
    artwork: List(MetadataArtwork),
  )
}

pub fn new() -> MediaMetadata {
  MediaMetadata("", "", "", [])
}

pub fn get_title(metadata: MediaMetadata) -> String {
  metadata.title
}

pub fn get_artist(metadata: MediaMetadata) -> String {
  metadata.artist
}

pub fn get_album(metadata: MediaMetadata) -> String {
  metadata.album
}

pub fn get_artwork(metadata: MediaMetadata) -> List(MetadataArtwork) {
  metadata.artwork
}

pub fn set_title(metadata: MediaMetadata, title: String) {
  MediaMetadata(..metadata, title: title)
}

pub fn set_artist(metadata: MediaMetadata, artist: String) {
  MediaMetadata(..metadata, artist: artist)
}

pub fn set_album(metadata: MediaMetadata, album: String) {
  MediaMetadata(..metadata, album: album)
}

pub fn set_artwork(metadata: MediaMetadata, artwork: List(MetadataArtwork)) {
  MediaMetadata(..metadata, artwork: artwork)
}
