//// Bindings to the MediaSession API.
////
//// The MediaSession API can be used to show and control details of currently
//// playing media in an OS specific dialog, such as a device lockscreen. It
//// also allows controlling the media using device interfaces such as media
//// buttons.
////
//// https://developer.mozilla.org/en-US/docs/Web/API/MediaSession

import plinth/browser/media/metadata.{type MediaMetadata}
import plinth/browser/media/action.{type Action, type ActionData}
import plinth/browser/media/position.{type PositionState}

/// Get the current media's metadata.
@external(javascript, "../../../media_ffi.mjs", "get")
pub fn get_metadata() -> MediaMetadata

/// Set the current media's metadata.
@external(javascript, "../../../media_ffi.mjs", "set")
pub fn set_metadata(metadata: MediaMetadata) -> Nil

/// Set the current position state.
@external(javascript, "../../../media_ffi.mjs", "setPositionState")
pub fn set_position_state(new_state: PositionState) -> Nil

/// Register an action handler to be called when the user triggers some action
/// in the UI or physical controls.
@external(javascript, "../../../media_ffi.mjs", "setActionHandler")
pub fn set_action_handler(action: Action, handler: fn(ActionData) -> Nil) -> Nil
