export function requestAnimationFrame(callback) {
  globalThis.requestAnimationFrame(callback);
}

export function replaceHash(hash) {
  globalThis.history.replaceState({}, document.title, hash);
}
