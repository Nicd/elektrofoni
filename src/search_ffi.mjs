export function getElem(id) {
  return document.getElementById(id);
}

export function focus(elem) {
  elem.focus();
  elem.selectionStart = elem.value.length;
  elem.selectionEnd = elem.value.length;
}
