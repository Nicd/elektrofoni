import gleam/javascript/promise
import gleam/json
import gleam/dict.{type Dict}
import gleam/dynamic
import gleam/result
import gleam/option
import ibroadcast/servers
import ibroadcast/request.{DecodeFailed}
import ibroadcast/authed_request.{type RequestConfig}
import ibroadcast/request_params.{type RequestParams}
import ibroadcast/http.{type Requestor}
import ibroadcast/library_format

pub type Album {
  Album(
    name: String,
    tracks: List(Int),
    artist_id: Int,
    trashed: Bool,
    rating: Int,
    disc: Int,
    year: Int,
  )
}

pub type Artist {
  Artist(
    name: String,
    tracks: List(Int),
    trashed: Bool,
    rating: Int,
    artwork_id: option.Option(String),
  )
}

pub type Track {
  Track(
    number: Int,
    year: Int,
    title: String,
    genre: String,
    length: Int,
    album_id: Int,
    artwork_id: Int,
    artist_id: Int,
    enid: Int,
    uploaded_on: String,
    trashed: Bool,
    size: Int,
    path: String,
    uid: String,
    rating: Int,
    plays: Int,
    file: String,
    type_: String,
    replay_gain: String,
    uploaded_time: String,
  )
}

pub type Library {
  Library(
    albums: Dict(Int, Album),
    artists: Dict(Int, Artist),
    tracks: Dict(Int, Track),
  )
}

pub type Settings {
  Settings(artwork_server: String, streaming_server: String)
}

pub type ResponseData {
  ResponseData(library: Library, settings: Settings)
}

pub fn get_library(config: RequestConfig, requestor: Requestor(err_type)) {
  use resp <- promise.try_await(authed_request.authed_request(
    servers.library,
    request_params(),
    config,
    requestor,
  ))

  promise.resolve(
    json.decode(resp.body, payload_decoder())
    |> result.map_error(DecodeFailed),
  )
}

fn request_params() -> RequestParams {
  [#("mode", json.string("library"))]
}

fn payload_decoder() {
  dynamic.decode2(
    ResponseData,
    dynamic.field("library", library_decoder()),
    dynamic.field("settings", settings_decoder()),
  )
}

fn library_decoder() {
  dynamic.decode3(
    Library,
    dynamic.field("albums", albums_decoder()),
    dynamic.field("artists", artists_decoder()),
    dynamic.field("tracks", tracks_decoder()),
  )
}

fn settings_decoder() {
  dynamic.decode2(
    Settings,
    dynamic.field("artwork_server", dynamic.string),
    dynamic.field("streaming_server", dynamic.string),
  )
}

fn albums_decoder() {
  library_format.decoder(library_format.string_int_decoder(), album_decoder())
}

fn album_decoder() {
  dynamic.decode7(
    Album,
    dynamic.element(0, dynamic.string),
    dynamic.element(1, dynamic.list(dynamic.int)),
    dynamic.element(2, dynamic.int),
    dynamic.element(3, dynamic.bool),
    dynamic.element(4, dynamic.int),
    dynamic.element(5, dynamic.int),
    dynamic.element(6, dynamic.int),
  )
}

fn artists_decoder() {
  library_format.decoder(library_format.string_int_decoder(), artist_decoder())
}

fn artist_decoder() {
  dynamic.decode5(
    Artist,
    dynamic.element(0, dynamic.string),
    dynamic.element(1, dynamic.list(dynamic.int)),
    dynamic.element(2, dynamic.bool),
    dynamic.element(3, dynamic.int),
    dynamic.any([
      dynamic.element(4, dynamic.optional(dynamic.string)),
      fn(_) { Ok(option.None) },
    ]),
  )
}

fn tracks_decoder() {
  library_format.decoder(library_format.string_int_decoder(), track_decoder())
}

fn track_decoder() {
  fn(data: dynamic.Dynamic) {
    use number <- result.try(dynamic.element(0, dynamic.int)(data))
    use year <- result.try(dynamic.element(1, dynamic.int)(data))
    use title <- result.try(dynamic.element(2, dynamic.string)(data))
    use genre <- result.try(dynamic.element(3, dynamic.string)(data))
    use length <- result.try(dynamic.element(4, dynamic.int)(data))
    use album_id <- result.try(dynamic.element(
      5,
      library_format.string_or_int_decoder(),
    )(data))
    use artwork_id <- result.try(dynamic.element(
      6,
      library_format.string_or_int_decoder(),
    )(data))
    use artist_id <- result.try(dynamic.element(
      7,
      library_format.string_or_int_decoder(),
    )(data))
    use enid <- result.try(dynamic.element(8, dynamic.int)(data))
    use uploaded_on <- result.try(dynamic.element(9, dynamic.string)(data))
    use trashed <- result.try(dynamic.element(10, dynamic.bool)(data))
    use size <- result.try(dynamic.element(11, dynamic.int)(data))
    use path <- result.try(dynamic.element(12, dynamic.string)(data))
    use uid <- result.try(dynamic.element(13, dynamic.string)(data))
    use rating <- result.try(dynamic.element(14, dynamic.int)(data))
    use plays <- result.try(dynamic.element(15, dynamic.int)(data))
    use file <- result.try(dynamic.element(16, dynamic.string)(data))
    use type_ <- result.try(dynamic.element(17, dynamic.string)(data))
    use replay_gain <- result.try(dynamic.element(18, dynamic.string)(data))
    use uploaded_time <- result.try(dynamic.element(19, dynamic.string)(data))

    Ok(Track(
      number: number,
      year: year,
      title: title,
      genre: genre,
      length: length,
      album_id: album_id,
      artwork_id: artwork_id,
      artist_id: artist_id,
      enid: enid,
      uploaded_on: uploaded_on,
      trashed: trashed,
      size: size,
      path: path,
      uid: uid,
      rating: rating,
      plays: plays,
      file: file,
      type_: type_,
      replay_gain: replay_gain,
      uploaded_time: uploaded_time,
    ))
  }
}
