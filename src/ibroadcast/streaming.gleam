import gleam/string
import gleam/int
import gleam/uri
import gleam/option
import ibroadcast/authed_request.{type RequestConfig}

pub fn form_url(
  track_file: String,
  track_id: Int,
  config: RequestConfig,
  server: String,
  bitrate: Int,
  expires: Int,
) {
  let assert Ok(url) = uri.parse(server)
  uri.Uri(
    ..url,
    path: string.replace(
      track_file,
      "/128/",
      "/" <> int.to_string(bitrate) <> "/",
    ),
    query: option.Some(uri.query_to_string([
      #("Expires", int.to_string(expires)),
      #("Signature", config.auth_info.token),
      #("file_id", int.to_string(track_id)),
      #("user_id", int.to_string(config.auth_info.user_id)),
      #("platform", config.base_config.app_info.client),
      #("version", config.base_config.app_info.version),
    ])),
  )
}
