import gleam/option.{None, Some}
import gleam/uri.{Uri}

pub const api = Uri(
  scheme: Some("https"),
  userinfo: None,
  host: Some("api.ibroadcast.com"),
  port: Some(443),
  path: "/",
  query: None,
  fragment: None,
)

pub const library = Uri(
  scheme: Some("https"),
  userinfo: None,
  host: Some("library.ibroadcast.com"),
  port: Some(443),
  path: "/",
  query: None,
  fragment: None,
)
