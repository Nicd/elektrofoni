import gleam/dict
import gleam/list
import ibroadcast/request_params.{type RequestParams}

/// Combine two request parameter lists into one, later values override former.
pub fn combine_params(params: List(RequestParams)) -> RequestParams {
  params
  |> list.flatten()
  |> list.fold(dict.new(), fn(acc, item) {
    let #(key, val) = item
    dict.insert(acc, key, val)
  })
  |> dict.to_list()
}
