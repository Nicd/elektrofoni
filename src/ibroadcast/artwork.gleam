//// Artwork is fetched from the artwork API server, whose address can be found
//// in the library response.

/// The desired size of the image in pixels square.
pub type Size {
  S150
  S300
  S1000
}

/// Get the URL to an artwork.
pub fn url(artwork_server: String, artwork_id: String, size: Size) {
  artwork_server <> "/artwork/" <> artwork_id <> "-" <> size_arg(size)
}

fn size_arg(size: Size) {
  case size {
    S150 -> "150"
    S300 -> "300"
    S1000 -> "1000"
  }
}
