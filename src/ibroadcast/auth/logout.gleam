import gleam/json
import ibroadcast/request_params.{type RequestParams}

pub fn request_params() -> RequestParams {
  [#("mode", json.string("logout"))]
}
