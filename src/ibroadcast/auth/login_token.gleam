import gleam/dynamic
import gleam/result
import gleam/json
import gleam/javascript/promise
import ibroadcast/request_params.{type RequestParams}
import ibroadcast/request.{type RequestConfig, type ResponseError, DecodeFailed}
import ibroadcast/http.{type Requestor}
import ibroadcast/servers

pub type Session {
  Session(user_id: Int, session_uuid: String)
}

pub type User {
  User(id: String, token: String, session: Session)
}

pub type ResponseData {
  AuthData(user: User)
  Failed
}

pub fn log_in(
  app_id: Int,
  login_token: String,
  config: RequestConfig,
  requestor: Requestor(err_type),
) -> promise.Promise(Result(ResponseData, ResponseError(err_type))) {
  use resp <- promise.try_await(request.raw_request(
    servers.api,
    request_params(app_id, login_token),
    config,
    requestor,
  ))

  promise.resolve(
    json.decode(resp.body, log_in_payload_decoder)
    |> result.map_error(DecodeFailed),
  )
}

fn request_params(app_id: Int, login_token: String) -> RequestParams {
  [
    #("mode", json.string("login_token")),
    #("type", json.string("account")),
    #("app_id", json.int(app_id)),
    #("login_token", json.string(login_token)),
  ]
}

fn log_in_payload_decoder(
  data: dynamic.Dynamic,
) -> Result(ResponseData, List(dynamic.DecodeError)) {
  use result <- result.try(dynamic.field("result", dynamic.bool)(data))
  case result {
    True -> dynamic.decode1(AuthData, dynamic.field("user", user_decoder))(data)
    False -> Ok(Failed)
  }
}

fn user_decoder(
  data: dynamic.Dynamic,
) -> Result(User, List(dynamic.DecodeError)) {
  dynamic.decode3(
    User,
    dynamic.field("id", dynamic.string),
    dynamic.field("token", dynamic.string),
    dynamic.field("session", session_decoder),
  )(data)
}

fn session_decoder(
  data: dynamic.Dynamic,
) -> Result(Session, List(dynamic.DecodeError)) {
  dynamic.decode2(
    Session,
    dynamic.field("user_id", dynamic.int),
    dynamic.field("session_uuid", dynamic.string),
  )(data)
}
