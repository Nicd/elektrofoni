//// The history argument can be used to make changes to play history. There is
//// no separate endpoint for it in the API and in this implementation the
//// `status` endpoint is used.
////
//// See https://devguide.ibroadcast.com/?p=api#Play-History

import gleam/dict.{type Dict}
import gleam/json
import gleam/int
import gleam/list
import gleam/result
import gleam/dynamic
import gleam/option
import gleam/javascript/promise
import birl
import ibroadcast/request.{DecodeFailed}
import ibroadcast/authed_request.{type RequestConfig}
import ibroadcast/http.{type Requestor}
import ibroadcast/auth/status
import ibroadcast/request_params
import ibroadcast/servers
import ibroadcast/time

/// One play event.
pub type Event {
  /// The track was played at this time.
  Play(when: birl.Time)
  /// The track was skipped at this time.
  Skip(when: birl.Time)
}

pub type Events =
  Dict(String, List(Event))

/// Track plays that occurred on a given day. `plays` contains the totals keyed
/// by track ID, `detail` contains the details of played and skipped tracks,
/// again keyed by track ID.
pub type HistoryDay {
  HistoryDay(day: birl.Day, plays: Dict(String, Int), detail: Events)
}

pub type History =
  List(HistoryDay)

/// Set play history.
pub fn set_history(
  history: History,
  config: RequestConfig,
  requestor: Requestor(err_type),
) {
  use resp <- promise.try_await(authed_request.authed_request(
    servers.api,
    request_params(history),
    config,
    requestor,
  ))

  promise.resolve(
    json.decode(resp.body, payload_decoder())
    |> result.map_error(DecodeFailed),
  )
}

/// Get the request params for a history request (using the `status` endpoint).
pub fn request_params(history: History) -> request_params.RequestParams {
  let history = json.array(history, day_serializer)
  [#("history", history), ..status.request_params()]
}

/// Add an event to a given history day.
pub fn add_to_day(day: HistoryDay, track_id: String, event: Event) -> HistoryDay {
  let plays = case event {
    Play(_) -> {
      let old_times = result.unwrap(dict.get(day.plays, track_id), 0)
      dict.insert(day.plays, track_id, old_times + 1)
    }
    Skip(_) -> day.plays
  }

  let detail =
    dict.update(day.detail, track_id, fn(old_detail) {
      case old_detail {
        option.Some(old_detail) -> list.append(old_detail, [event])
        option.None -> [event]
      }
    })

  HistoryDay(..day, plays: plays, detail: detail)
}

fn payload_decoder() {
  dynamic.field("result", dynamic.bool)
}

fn day_serializer(day: HistoryDay) -> json.Json {
  json.object([
    #(
      "day",
      json.string(
        int.to_string(day.day.year)
          <> "-"
          <> {
            int.to_string(day.day.month)
            |> time.pad_time_part()
          }
          <> "-"
          <> {
            int.to_string(day.day.date)
            |> time.pad_time_part()
          },
      ),
    ),
    #(
      "plays",
      json.object(
        list.map(dict.to_list(day.plays), fn(play) {
          #(play.0, json.int(play.1))
        }),
      ),
    ),
    #(
      "detail",
      json.object(
        list.map(dict.to_list(day.detail), fn(item) {
          #(
            item.0,
            json.array(item.1, fn(event) {
              json.object([
                #("ts", json.string(time.format_timestamp(event.when))),
                case event {
                  Play(..) -> #("event", json.string("play"))
                  Skip(..) -> #("event", json.string("skip"))
                },
              ])
            }),
          )
        }),
      ),
    ),
  ])
}
