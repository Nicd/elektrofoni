import gleam/uri
import gleam/json
import ibroadcast/http.{type Requestor}
import ibroadcast/request_params.{type RequestParams}
import ibroadcast/request
import ibroadcast/utils

pub type AuthInfo {
  AuthInfo(user_id: Int, token: String)
}

pub type RequestConfig {
  RequestConfig(base_config: request.RequestConfig, auth_info: AuthInfo)
}

pub fn authed_params(auth_info: AuthInfo) -> RequestParams {
  [
    #("user_id", json.int(auth_info.user_id)),
    #("token", json.string(auth_info.token)),
  ]
}

pub fn authed_request(
  url: uri.Uri,
  params: RequestParams,
  config: RequestConfig,
  requestor: Requestor(err_type),
) {
  request.raw_request(
    url,
    utils.combine_params([authed_params(config.auth_info), params]),
    config.base_config,
    requestor,
  )
}
