import gleam/http/request
import gleam/http/response
import gleam/javascript/promise

/// The HTTP client to use in API calls.
pub type Requestor(err_type) =
  fn(request.Request(String)) ->
    promise.Promise(Result(response.Response(String), err_type))
