import gleam/json

pub type RequestParams =
  List(#(String, json.Json))
