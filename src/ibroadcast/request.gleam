import gleam/uri
import gleam/result
import gleam/http as gleam_http
import gleam/http/request
import gleam/javascript/promise
import gleam/json
import ibroadcast/utils
import ibroadcast/http.{type Requestor}
import ibroadcast/app_info.{type AppInfo}
import ibroadcast/device_info.{type DeviceInfo}
import ibroadcast/request_params.{type RequestParams}

pub type RequestConfig {
  RequestConfig(app_info: AppInfo, device_info: DeviceInfo)
}

pub type ResponseError(requestor_err_type) {
  RequestFailed(requestor_err_type)
  DecodeFailed(json.DecodeError)
}

pub fn base_params(config: RequestConfig) -> RequestParams {
  [
    #("client", json.string(config.app_info.client)),
    #("version", json.string(config.app_info.version)),
    #("device_name", json.string(config.device_info.name)),
    #("user_agent", json.string(config.device_info.user_agent)),
  ]
}

pub fn raw_request(
  url: uri.Uri,
  params: RequestParams,
  config: RequestConfig,
  requestor: Requestor(err_type),
) {
  let assert Ok(req) = request.from_uri(url)
  let params = utils.combine_params([base_params(config), params])
  let req =
    req
    |> request.set_body(json.to_string(json.object(params)))
    |> request.set_method(gleam_http.Post)
  requestor(req)
  |> promise.map(fn(r) { result.map_error(r, RequestFailed) })
}
