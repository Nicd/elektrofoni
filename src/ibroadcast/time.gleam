import gleam/int
import gleam/string
import birl

/// Format a timestamp according to the needs of the API.
/// The format is `2024-01-26 23:37:15` with no offset.
pub fn format_timestamp(time: birl.Time) -> String {
  let date_str = birl.to_naive_date_string(time)
  let time_of_day = birl.get_time_of_day(time)
  let hours_str =
    int.to_string(time_of_day.hour)
    |> pad_time_part()
  let mins_str =
    int.to_string(time_of_day.minute)
    |> pad_time_part()
  let secs_str =
    int.to_string(time_of_day.second)
    |> pad_time_part()

  date_str <> " " <> hours_str <> ":" <> mins_str <> ":" <> secs_str
}

/// Pad a time part (months, days, hours, minutes, seconds) to two characters.
pub fn pad_time_part(str: String) -> String {
  string.pad_left(str, to: 2, with: "0")
}
