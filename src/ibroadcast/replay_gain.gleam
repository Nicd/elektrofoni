import gleam/float

/// Calculate the replay gain factor based on the value stored for a track.
///
/// The resulting factor ranges from 0 to a positive number. It should be scaled
/// down using the master volume and then clamped to a range from 0 to 1.
pub fn factor(replay_gain: Float) -> Float {
  let assert Ok(factor) = float.power(10.0, replay_gain /. 20.0)
  factor
}
