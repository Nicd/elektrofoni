import gleam/string
import gleam/option
import gleam/result
import gleam/int
import ibroadcast/library/library.{type Artist as APIArtist}
import elekf/library/artist.{Artist}

/// Converts API artist response to library format.
pub fn from(artist: APIArtist) {
  Artist(
    name: artist.name,
    name_lower: string.lowercase(artist.name),
    tracks: artist.tracks,
    trashed: artist.trashed,
    rating: artist.rating,
    artwork_id: artist.artwork_id
    |> option.map(fn(id) { result.unwrap(int.parse(id), 0) }),
  )
}
