import gleam/list
import gleam/dict.{type Dict}
import ibroadcast/library/library.{type Library as APILibrary} as api_library
import elekf/library.{Library}
import elekf/transfer/album
import elekf/transfer/artist
import elekf/transfer/track

/// Converts API library response to library format.
pub fn from(library: APILibrary) {
  let albums = transfer_map(library.albums, album.from)
  let artists = transfer_map(library.artists, artist.from)
  let tracks = transfer_map(library.tracks, track.from)
  Library(albums: albums, artists: artists, tracks: tracks)
}

fn transfer_map(data: Dict(Int, a), transferrer: fn(a) -> b) -> Dict(Int, b) {
  data
  |> dict.to_list()
  |> list.fold(dict.new(), fn(acc, item) {
    dict.insert(acc, item.0, transferrer(item.1))
  })
}
