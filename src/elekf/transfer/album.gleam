import gleam/string
import ibroadcast/library/library.{type Album as APIAlbum}
import elekf/library/album.{Album}

/// Converts API album response to library format.
pub fn from(album: APIAlbum) {
  Album(
    name: album.name,
    name_lower: string.lowercase(album.name),
    tracks: album.tracks,
    artist_id: album.artist_id,
    trashed: album.trashed,
    rating: album.rating,
    disc: album.disc,
    year: album.year,
  )
}
