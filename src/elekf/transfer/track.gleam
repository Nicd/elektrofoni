import gleam/string
import gleam/option
import gleam/float
import gleam/result
import ibroadcast/library/library.{type Track as APITrack}
import elekf/library/track.{Track}

/// Converts API track response to library format.
pub fn from(track: APITrack) {
  let artwork_id = case track.artwork_id {
    0 -> option.None
    id -> option.Some(id)
  }

  Track(
    number: track.number,
    year: track.year,
    title: track.title,
    title_lower: string.lowercase(track.title),
    genre: track.genre,
    length: track.length,
    album_id: track.album_id,
    artwork_id: artwork_id,
    artist_id: track.artist_id,
    enid: track.enid,
    uploaded_on: track.uploaded_on,
    trashed: track.trashed,
    size: track.size,
    path: track.path,
    uid: track.uid,
    rating: track.rating,
    plays: track.plays,
    file: track.file,
    type_: track.type_,
    replay_gain: result.unwrap(float.parse(track.replay_gain), 1.0),
    uploaded_time: track.uploaded_time,
  )
}
