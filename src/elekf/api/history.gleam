//// History events are for changing the play history in the service. These will
//// also scrobble the tracks to Last.FM.

import gleam/int
import gleam/dict
import gleam/list
import gleam/result
import birl
import ibroadcast/authed_request.{type RequestConfig}
import ibroadcast/history as history_api
import ibroadcast/http.{type Requestor}
import elekf/library/track.{type Track}

/// Type of history event.
pub type HistoryEventType {
  /// The track was played.
  Play
  /// The track was skipped.
  Skip
}

/// An event that occurred for a track.
pub type HistoryEvent {
  HistoryEvent(
    track_id: Int,
    track: Track,
    type_: HistoryEventType,
    when: birl.Time,
  )
}

pub type History =
  List(HistoryEvent)

/// Send an update to the history API.
pub fn update_history(
  history: History,
  request_config: RequestConfig,
  requestor: Requestor(err_type),
) {
  history_api.set_history(to_api_format(history), request_config, requestor)
}

fn to_api_format(history: History) -> history_api.History {
  list.fold(history, dict.new(), fn(acc, event) {
    let day = birl.get_day(event.when)
    let type_constructor = case event.type_ {
      Play -> history_api.Play
      Skip -> history_api.Skip
    }
    let history_day =
      result.unwrap(
        dict.get(acc, day),
        history_api.HistoryDay(day: day, plays: dict.new(), detail: dict.new()),
      )
      |> history_api.add_to_day(
        int.to_string(event.track_id),
        type_constructor(event.when),
      )
    dict.insert(acc, day, history_day)
  })
  |> dict.values()
}
