import elektrofoni
import ibroadcast/device_info.{DeviceInfo}
import ibroadcast/request.{RequestConfig}
import elekf/utils/navigator

/// Gets the base request config to use for all requests, authenticated and raw.
pub fn base_request_config(device_name: String) {
  RequestConfig(
    app_info: elektrofoni.app_info,
    device_info: DeviceInfo(
      name: device_name,
      user_agent: navigator.user_agent(),
    ),
  )
}
