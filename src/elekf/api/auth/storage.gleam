//// The auth storage stores the user's authentication credentials for future
//// use, i.e. when refreshing the page.

import gleam/dynamic
import gleam/json
import plinth/javascript/storage
import varasto
import elekf/api/auth/models

const storage_key = "__elektrofoni_auth_storage"

/// Storage format of the authentication data in local storage.
pub type StorageFormat {
  StorageFormat(user: models.User, device: models.Device)
}

/// The local storage API used for storing authentication details.
pub type AuthStorage =
  varasto.TypedStorage(StorageFormat)

/// Gets the `varasto` instance to use for reading and writing auth storage.
pub fn get() {
  let assert Ok(local) = storage.local()
  varasto.new(local, reader(), writer())
}

/// Reads the previously stored value, if available.
pub fn read(storage: AuthStorage) {
  varasto.get(storage, storage_key)
}

/// Writes new value to the storage.
pub fn write(storage: AuthStorage, data: StorageFormat) {
  varasto.set(storage, storage_key, data)
}

fn reader() {
  dynamic.decode2(
    StorageFormat,
    dynamic.field("user", user_decoder()),
    dynamic.field("device", device_decoder()),
  )
}

fn writer() {
  fn(val: StorageFormat) {
    json.object([
      #(
        "user",
        json.object([
          #("id", json.int(val.user.id)),
          #("token", json.string(val.user.token)),
          #("session_uuid", json.string(val.user.session_uuid)),
        ]),
      ),
      #("device", json.object([#("name", json.string(val.device.name))])),
    ])
  }
}

fn user_decoder() {
  dynamic.decode3(
    models.User,
    dynamic.field("id", dynamic.int),
    dynamic.field("token", dynamic.string),
    dynamic.field("session_uuid", dynamic.string),
  )
}

fn device_decoder() {
  dynamic.decode1(models.Device, dynamic.field("name", dynamic.string))
}
