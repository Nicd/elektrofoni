/// Information of the logged in user.
pub type User {
  User(id: Int, token: String, session_uuid: String)
}

/// Information of the user's device.
pub type Device {
  Device(name: String)
}
