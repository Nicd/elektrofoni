//// The library stores the music library retrieved from iBroadcast.

import gleam/dict.{type Dict}
import elekf/library/track.{type Track}
import elekf/library/album.{type Album}
import elekf/library/artist.{type Artist}

pub type Library {
  Library(
    albums: Dict(Int, Album),
    artists: Dict(Int, Artist),
    tracks: Dict(Int, Track),
  )
}

/// An ID that can never match any library item
pub const invalid_id = -1

/// Gets an empty library for use as a default value.
pub fn empty() {
  Library(albums: dict.new(), artists: dict.new(), tracks: dict.new())
}

/// Gets an album from the library based on ID.
pub fn get_album(library: Library, id: Int) {
  dict.get(library.albums, id)
}

/// Gets an artist from the library based on ID.
pub fn get_artist(library: Library, id: Int) {
  dict.get(library.artists, id)
}

/// Gets a track from the library based on ID.
pub fn get_track(library: Library, id: Int) {
  dict.get(library.tracks, id)
}

/// Gets an album from the library, asserting that it exists.
pub fn assert_album(library: Library, id: Int) {
  let assert Ok(album) = dict.get(library.albums, id)
  album
}

/// Gets an artist from the library, asserting that it exists.
pub fn assert_artist(library: Library, id: Int) {
  let assert Ok(artist) = dict.get(library.artists, id)
  artist
}

/// Gets a track from the library, asserting that it exists.
pub fn assert_track(library: Library, id: Int) {
  let assert Ok(track) = dict.get(library.tracks, id)
  track
}
