import gleam/option

/// A track in the music library.
///
/// The `title_lower` contains the track title in lowercase, which is an
/// optimisation for searching.
pub type Track {
  Track(
    number: Int,
    year: Int,
    title: String,
    title_lower: String,
    genre: String,
    length: Int,
    album_id: Int,
    artwork_id: option.Option(Int),
    artist_id: Int,
    enid: Int,
    uploaded_on: String,
    trashed: Bool,
    size: Int,
    path: String,
    uid: String,
    rating: Int,
    plays: Int,
    file: String,
    type_: String,
    replay_gain: Float,
    uploaded_time: String,
  )
}
