import gleam/option

pub type Artist {
  Artist(
    name: String,
    name_lower: String,
    tracks: List(Int),
    trashed: Bool,
    rating: Int,
    artwork_id: option.Option(Int),
  )
}
