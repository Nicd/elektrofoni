import gleam/string
import elekf/library/artist.{type Artist}

/// Sort given artists by their lowercased names.
pub fn sort_by_name(a: Artist, b: Artist) {
  string.compare(a.name_lower, b.name_lower)
}
