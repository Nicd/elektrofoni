import gleam/string
import gleam/int
import elekf/utils/order
import elekf/library/track.{type Track}

const track_number_sorters = [order_compare, name_compare]

/// Sort given tracks by their lowercased names.
pub fn sort_by_name(a: Track, b: Track) {
  name_compare(a, b)
}

/// Sort given tracks by their track number first, lowercased title second.
pub fn sort_by_track_number(a: Track, b: Track) {
  order.compare_by_multiple(track_number_sorters, a, b)
}

fn order_compare(a: Track, b: Track) {
  int.compare(a.number, b.number)
}

fn name_compare(a: Track, b: Track) {
  string.compare(a.title_lower, b.title_lower)
}
