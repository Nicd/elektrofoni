pub type Album {
  Album(
    name: String,
    name_lower: String,
    tracks: List(Int),
    artist_id: Int,
    trashed: Bool,
    rating: Int,
    disc: Int,
    year: Int,
  )
}
