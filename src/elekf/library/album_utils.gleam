import gleam/list
import gleam/string
import gleam/int
import elekf/utils/order
import elekf/library.{type Library}
import elekf/library/album.{type Album}

const year_sorters = [year_compare, name_compare]

/// Get the individual tracks in an album.
pub fn get_tracks(library: Library, album: Album) {
  list.map(
    album.tracks,
    fn(track_id) { #(track_id, library.assert_track(library, track_id)) },
  )
}

/// Sort given albums by their lowercased names.
pub fn sort_by_name(a: Album, b: Album) {
  name_compare(a, b)
}

/// Sort given albums by their publishing year first, then lowercased names.
pub fn sort_by_year(a: Album, b: Album) {
  order.compare_by_multiple(year_sorters, a, b)
}

fn name_compare(a: Album, b: Album) {
  string.compare(a.name_lower, b.name_lower)
}

fn year_compare(a: Album, b: Album) {
  int.compare(a.year, b.year)
}
