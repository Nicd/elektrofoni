import gleam/dynamic

pub type CustomEvent

@external(javascript, "../../custom_event_ffi.mjs", "newEvent")
pub fn new(name: String, data: any) -> CustomEvent

@external(javascript, "../../custom_event_ffi.mjs", "getDetail")
pub fn get_detail(e: CustomEvent) -> Result(dynamic.Dynamic, Nil)
