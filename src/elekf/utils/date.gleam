import gleam/result
import gleam/order.{type Order, Eq, Gt, Lt}
import gleam/dynamic.{type DecodeError, type Dynamic, DecodeError}

pub type Date

@external(javascript, "../../date_ffi.mjs", "from_iso8601")
pub fn from_iso8601(a: String) -> Result(Date, Nil)

@external(javascript, "../../date_ffi.mjs", "to_iso8601")
pub fn to_iso8601(d: Date) -> String

@external(javascript, "../../date_ffi.mjs", "from_unix")
pub fn from_unix(a: Int) -> Result(Date, Nil)

@external(javascript, "../../date_ffi.mjs", "to_unix")
pub fn to_unix(a: Date) -> Int

@external(javascript, "../../date_ffi.mjs", "now")
pub fn now() -> Date

@external(javascript, "../../date_ffi.mjs", "unix_now")
pub fn unix_now() -> Int

@external(javascript, "../../date_ffi.mjs", "equals")
pub fn equals(a: Date, b: Date) -> Bool

@external(javascript, "../../date_ffi.mjs", "bigger_than")
pub fn bigger_than(a: Date, b: Date) -> Bool

/// Compares given dates, returning `Gt` if `a` > `b`
pub fn compare(a: Date, b: Date) -> Order {
  case equals(a, b) {
    True -> Eq
    False -> {
      case bigger_than(a, b) {
        True -> Gt
        False -> Lt
      }
    }
  }
}

/// Decodes a dynamic value as an ISO 8601 formatted datetime string.
pub fn decode(value: Dynamic) -> Result(Date, List(DecodeError)) {
  use str <- result.try(dynamic.string(value))
  use date <- result.try(
    from_iso8601(str)
    |> result.replace_error([DecodeError("ISO 8601 formatted string", str, [])]),
  )
  Ok(date)
}
