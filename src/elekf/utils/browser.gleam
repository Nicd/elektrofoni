@external(javascript, "../../browser_ffi.mjs", "requestAnimationFrame")
pub fn request_animation_frame(callback: fn() -> Nil) -> Nil

@external(javascript, "../../browser_ffi.mjs", "replaceHash")
pub fn replace_hash(new_url: String) -> Nil
