import gleam/option

/// Asserts that an option contains a value, and returns that value.
pub fn assert_some(val: option.Option(a)) -> a {
  let assert option.Some(content) = val
  content
}
