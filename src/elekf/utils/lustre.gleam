import lustre/element
import elekf/utils/browser

/// Run the given callback after the next render has occurred.
pub fn after_next_render(callback: fn() -> Nil) -> Nil {
  browser.request_animation_frame(callback)
}

/// Render an element if the condition is true, otherwise render nothing
pub fn if_(condition: Bool, content: fn() -> element.Element(msg)) {
  case condition {
    True -> content()
    False -> element.text("")
  }
}
