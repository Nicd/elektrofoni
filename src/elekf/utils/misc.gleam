//// Miscellaneous utilities that do not fit anywhere else.

/// Toggle a value with a new value - if the new value is the same as the
/// previous, returns the "null" value. Otherwise returns the new value.
pub fn toggle(prev: a, new: a, null: a) {
  case new == prev {
    True -> null
    False -> new
  }
}
