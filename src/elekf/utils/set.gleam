import gleam/set.{type Set}

/// Toggle an item in a set. In other words, insert the item if it didn't
/// already exist, and remove it if it did.
pub fn toggle(set: Set(a), item: a) -> Set(a) {
  case set.contains(set, item) {
    True -> set.delete(set, item)
    False -> set.insert(set, item)
  }
}
