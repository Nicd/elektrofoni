/// Returns the current browser's user agent.
@external(javascript, "../../navigator_ffi.mjs", "userAgent")
pub fn user_agent() -> String
