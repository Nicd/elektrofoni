import gleam/fetch
import gleam/http/request
import gleam/javascript/promise
import ibroadcast/request as ibroadcast_request
import ibroadcast/http as ibroadcast_http

/// Error returned by an HTTP request.
pub type ResponseError =
  ibroadcast_request.ResponseError(fetch.FetchError)

/// Returns an HTTP request function based on `fetch`.
pub fn requestor() -> ibroadcast_http.Requestor(fetch.FetchError) {
  fn(req: request.Request(String)) {
    use resp <- promise.try_await(fetch.send(req))
    use resp <- promise.try_await(fetch.read_text_body(resp))
    promise.resolve(Ok(resp))
  }
}
