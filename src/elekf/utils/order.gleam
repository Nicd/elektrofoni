import gleam/order
import gleam/list

/// A function that sorts two values.
pub type Sorter(a) =
  fn(a, a) -> order.Order

/// Compare two values by using multiple sorters. The comparison will stop after
/// the first sorter that returns something other than `order.Eq`.
pub fn compare_by_multiple(sorters: List(Sorter(a)), a: a, b: a) {
  list.fold_until(sorters, order.Eq, fn(prev, sorter) {
    case prev {
      order.Eq -> list.Continue(sorter(a, b))
      other -> list.Stop(other)
    }
  })
}

/// A sorter that does not sort, keeping the order of the elements.
pub fn noop(_a, _b) {
  order.Eq
}
