import gleam/result
import gleam/dynamic
import lustre/event
import elekf/web/common.{type PlayQueue}
import elekf/utils/custom_event.{type CustomEvent}

const event_name = "start-play"

pub type EventData {
  EventData(tracks: PlayQueue, position: Int)
}

pub fn emit(tracks: PlayQueue, position: Int) {
  event.emit(event_name, EventData(tracks, position))
}

pub fn on(msg: fn(PlayQueue, Int) -> b) {
  event.on(event_name, fn(data) {
    data
    |> decoder
    |> result.map(fn(e) { msg(e.tracks, e.position) })
  })
}

fn decoder(data: dynamic.Dynamic) {
  let e: CustomEvent = dynamic.unsafe_coerce(data)
  let assert Ok(detail) = custom_event.get_detail(e)
  let event_data: EventData =
    detail
    |> dynamic.unsafe_coerce()

  Ok(event_data)
}
