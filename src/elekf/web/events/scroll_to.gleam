import gleam/result
import gleam/dynamic
import lustre/event
import elekf/utils/custom_event.{type CustomEvent}

pub const event_name = "scroll-to"

pub type EventData {
  EventData(pos: Float)
}

pub fn emit(pos: Float) {
  event.emit(event_name, EventData(pos))
}

pub fn on(msg: fn(Float) -> b) {
  event.on(event_name, fn(data) {
    data
    |> decoder
    |> result.map(fn(e) { msg(e.pos) })
  })
}

pub fn js_custom_event(pos: Float) {
  custom_event.new(event_name, EventData(pos))
}

fn decoder(data: dynamic.Dynamic) {
  let e: CustomEvent = dynamic.unsafe_coerce(data)
  use detail <- result.try(custom_event.get_detail(e))
  use event_data <- result.try(
    data_decoder()(detail)
    |> result.nil_error(),
  )

  Ok(event_data)
}

fn data_decoder() {
  dynamic.decode1(EventData, dynamic.field("pos", dynamic.float))
}
