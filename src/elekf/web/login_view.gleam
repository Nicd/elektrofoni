//// The login view displays the login form and starts the login operation.

import gleam/string
import gleam/javascript/promise
import lustre/element.{text}
import lustre/element/html.{button, form, hr, input, p}
import lustre/attribute
import lustre/event
import lustre/effect
import ibroadcast/auth/login_token
import elektrofoni
import elekf/api/base_request_config.{base_request_config}
import elekf/utils/http
import elekf/utils/lustre

pub type Model {
  Model(
    device_name: String,
    login_token: String,
    logging_in: Bool,
    login_failed: Bool,
    error: String,
  )
}

pub type Msg {
  AttemptLogin
  LoginResult(Result(login_token.ResponseData, http.ResponseError))
  OnDeviceNameInput(String)
  OnLoginTokenInput(String)
}

pub fn init() {
  Model("", "", False, False, "")
}

pub fn update(model, msg) {
  case msg {
    OnDeviceNameInput(name) -> #(
      Model(..model, device_name: name),
      effect.none(),
    )
    OnLoginTokenInput(token) -> #(
      Model(..model, login_token: token),
      effect.none(),
    )
    AttemptLogin -> #(Model(..model, logging_in: True), start_login(model))
    LoginResult(Ok(login_token.Failed)) -> #(
      Model(
        ..model,
        login_failed: True,
        error: "The credentials were not accepted.",
      ),
      effect.none(),
    )
    LoginResult(Error(error)) -> #(
      Model(
        ..model,
        logging_in: False,
        login_failed: True,
        error: "An error prevented logging in: "
        <> string.inspect(error),
      ),
      effect.none(),
    )
    _ -> {
      panic as { "Login view got unknown message: " <> string.inspect(msg) }
    }
  }
}

pub fn view(model: Model) {
  form(
    [
      attribute.attribute("method", "post"),
      attribute.id("login-form"),
      event.on_submit(AttemptLogin),
    ],
    [
      input([
        attribute.id("device-name"),
        attribute.name("device-name"),
        attribute.placeholder("Device name (i.e. Kalle's Fairphone)"),
        event.on_input(OnDeviceNameInput),
      ]),
      input([
        attribute.id("login-token"),
        attribute.name("login-token"),
        attribute.placeholder("Login token"),
        event.on_input(OnLoginTokenInput),
      ]),
      button(
        [
          attribute.type_("submit"),
          attribute.disabled(
            model.logging_in
            || model.device_name == ""
            || model.login_token == "",
          ),
        ],
        [text("Log in")],
      ),
      lustre.if_(model.login_failed, fn() {
        p([attribute.class("form-error")], [
          text("Logging in failed due to: " <> model.error),
        ])
      }),
      hr([]),
      p([attribute.class("form-info")], [
        text(
          "To log in, obtain your login token from iBroadcast by going to Menu -> Apps.",
        ),
      ]),
    ],
  )
}

fn start_login(model: Model) {
  use dispatch <- effect.from()

  login_token.log_in(
    elektrofoni.app_id,
    model.login_token,
    base_request_config(model.device_name),
    http.requestor(),
  )
  |> promise.map(LoginResult)
  |> promise.tap(dispatch)

  Nil
}
