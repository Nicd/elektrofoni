//// The authed view manages the user's request config and displays the base
//// view for the app.

import gleam/io
import gleam/list
import gleam/option
import gleam/float
import gleam/int
import gleam/result
import gleam/set
import gleam/javascript/promise
import lustre/element.{text}
import lustre/element/html.{div, nav, p}
import lustre/attribute
import lustre/effect
import lustre/event
import birl
import ibroadcast/library/library as library_api
import ibroadcast/authed_request.{type RequestConfig, RequestConfig}
import elekf/web/common.{type PlayQueue}
import elekf/api/base_request_config.{base_request_config}
import elekf/api/history
import elekf/utils/http
import elekf/utils/lustre
import elekf/library.{type Library}
import elekf/library/artist.{type Artist}
import elekf/library/track.{type Track}
import elekf/transfer/library as library_transfer
import elekf/web/router
import elekf/web/components/player
import elekf/web/components/player/model as player_model
import elekf/web/components/player/actions as player_actions
import elekf/web/components/library_view
import elekf/web/components/library_views/tracks_view
import elekf/web/components/library_views/artists_view
import elekf/web/components/library_views/albums_view
import elekf/web/components/library_views/single_artist_view
import elekf/web/components/library_views/single_album_view
import elekf/web/components/button_group
import elekf/web/components/link
import elekf/web/events/start_play
import elekf/web/utils
import elekf/utils/browser
import elekf/web/components/icon.{Alt, icon}
import elektrofoni

/// Status of the current track that is being played.
pub type CurrentTrackStatus {
  /// We are tracking the play status.
  Tracking(played: Float, last_time: Float)
  /// History status update has been sent for this track.
  HistorySent
}

pub type PlayInfo {
  PlayInfo(
    track_id: Int,
    track: Track,
    play_queue: PlayQueue,
    play_index: Int,
    player: player_model.Model,
    current_track_status: CurrentTrackStatus,
  )
}

pub type PlayStatus {
  HasTracks(info: PlayInfo)
  NoTracks
}

pub type Model {
  Model(
    loading_library: Bool,
    library: Library,
    settings: option.Option(common.Settings),
    request_config: RequestConfig,
    play_status: PlayStatus,
    view: library_view.View,
  )
}

pub type Msg {
  UpdateAuthData(common.AuthData)
  LibraryResult(Result(library_api.ResponseData, http.ResponseError))
  PlayerMsg(player.Msg)
  StartPlay(PlayQueue, Int)
  Router(router.Msg)
  LibraryViewScrollToTopRequested
}

pub fn init(auth_data: common.AuthData) {
  let initial_library = library.empty()

  let router_effect =
    effect.from(fn(dispatch) { router.init(dispatch) })
    |> effect.map(Router)

  let #(path, query) = router.get_current_path()
  let route = result.unwrap(router.parse(path), router.default_route)

  // If we are initialising straight into the full screen player, remove the
  // query argument (as the player is not open on init)
  case set.contains(router.parse_query(query), router.FullScreen) {
    True ->
      browser.replace_hash(
        router.queryless(route)
        |> router.to_hash(),
      )
    False -> Nil
  }

  let initial_view =
    route
    |> route_to_view()
    |> result.unwrap(library_view.Tracks)

  let model =
    Model(
      loading_library: True,
      library: initial_library,
      settings: option.None,
      request_config: form_request_config(auth_data),
      play_status: NoTracks,
      view: initial_view,
    )

  #(model, effect.batch([load_library(model), router_effect]))
}

pub fn update(model: Model, msg) {
  case msg {
    UpdateAuthData(auth_data) -> {
      let new_config = form_request_config(auth_data)

      let #(play_status, player_effect) = case model.play_status {
        HasTracks(PlayInfo(player: p, ..) as play_info) -> {
          let #(new_model, e) =
            utils.update_child(
              p,
              player.UpdateRequestConfig(new_config),
              player.update,
              PlayerMsg,
            )
          #(HasTracks(PlayInfo(..play_info, player: new_model)), e)
        }
        NoTracks -> #(NoTracks, effect.none())
      }
      #(
        Model(..model, request_config: new_config, play_status: play_status),
        player_effect,
      )
    }
    LibraryResult(Ok(data)) -> {
      let settings =
        common.Settings(
          artwork_server: data.settings.artwork_server,
          streaming_server: data.settings.streaming_server,
        )
      #(
        Model(
          ..model,
          loading_library: False,
          library: library_transfer.from(data.library),
          settings: option.Some(settings),
        ),
        effect.none(),
      )
    }
    LibraryResult(Error(error)) -> {
      io.println_error("Library load failed:")
      io.debug(error)
      #(Model(..model, loading_library: False), effect.none())
    }
    StartPlay(tracks, position) -> {
      let #(status, effect) = handle_start_play(model, tracks, position)
      #(Model(..model, play_status: status), effect)
    }
    PlayerMsg(player.ActionTriggered(player_actions.NextTrack))
    | PlayerMsg(player.ActionTriggered(player_actions.PrevTrack)) -> {
      if_player(model, fn(info) {
        let next_index = case msg {
          PlayerMsg(player.ActionTriggered(player_actions.NextTrack)) ->
            info.play_index + 1
          _ -> info.play_index - 1
        }
        case list.at(info.play_queue, next_index) {
          Ok(_) -> {
            let #(status, effect) =
              handle_start_play(model, info.play_queue, next_index)
            #(Model(..model, play_status: status), effect)
          }
          Error(_) -> #(model, effect.none())
        }
      })
    }
    PlayerMsg(msg) -> {
      if_player(model, fn(info) {
        let #(player_model, player_effect) =
          utils.update_child(info.player, msg, player.update, PlayerMsg)

        let play_info =
          update_play_info(info, msg)
          |> maybe_send_history_status(model.request_config)

        #(
          Model(
            ..model,
            play_status: HasTracks(PlayInfo(..play_info, player: player_model)),
          ),
          player_effect,
        )
      })
    }
    Router(router.RouteChanged(route_and_query)) -> {
      case route_to_view(route_and_query.route) {
        Ok(view) -> {
          let view_updated = Model(..model, view: view)
          if_player(view_updated, fn(info) {
            let msg = case
              set.contains(route_and_query.query, router.FullScreen)
            {
              True -> player.FullScreenOpened
              False -> player.FullScreenClosed
            }

            let #(player_model, e) =
              utils.update_child(info.player, msg, player.update, PlayerMsg)

            #(
              Model(
                ..view_updated,
                play_status: HasTracks(PlayInfo(..info, player: player_model)),
              ),
              e,
            )
          })
        }
        Error(_) -> {
          io.println_error("Unable to change to route:")
          io.debug(route_and_query)
          #(model, effect.none())
        }
      }
    }

    LibraryViewScrollToTopRequested -> {
      library_view.request_scroll(0.0)
      #(model, effect.none())
    }
  }
}

pub fn view(model: Model) {
  let player_open_attribute = case model.play_status {
    HasTracks(_) -> attribute.attribute("data-player-status", "open")
    NoTracks -> attribute.attribute("data-player-status", "closed")
  }

  div([attribute.id("authed-view-wrapper"), player_open_attribute], [
    div([attribute.id("library-loading-indicator")], [
      lustre.if_(model.loading_library, fn() {
        p([], [text("Loading library…")])
      }),
    ]),
    div([attribute.id("authed-view-library")], [
      nav([attribute.id("library-top-nav")], [
        button_group.view("", [], [
          link.button(
            router.to_hash(router.queryless(router.TrackList)),
            "",
            [maybe_scroll(model.view, library_view.Tracks)],
            [icon("music-note-beamed", Alt("Tracks"))],
          ),
          link.button(
            router.to_hash(router.queryless(router.ArtistList)),
            "",
            [maybe_scroll(model.view, library_view.Artists)],
            [icon("file-person", Alt("Artists"))],
          ),
          link.button(
            router.to_hash(router.queryless(router.AlbumList)),
            "",
            [maybe_scroll(model.view, library_view.Albums)],
            [icon("disc", Alt("Albums"))],
          ),
        ]),
      ]),
      case model.view {
        library_view.Tracks ->
          tracks_view.render(library_if_loaded(model), model.settings, [
            attribute.id("tracks-view"),
            attribute.class("glass-bg"),
            start_play.on(StartPlay),
          ])
        library_view.Artists ->
          artists_view.render(library_if_loaded(model), model.settings, [
            attribute.id("artists-view"),
            attribute.class("glass-bg"),
            start_play.on(StartPlay),
          ])
        library_view.Albums ->
          albums_view.render(library_if_loaded(model), model.settings, [
            attribute.id("albums-view"),
            attribute.class("glass-bg"),
            start_play.on(StartPlay),
          ])
        library_view.SingleArtist(id) ->
          single_artist_view.render(
            library_if_loaded(model),
            id,
            model.settings,
            [
              attribute.id("single-artist-view"),
              attribute.class("glass-bg"),
              start_play.on(StartPlay),
            ],
          )
        library_view.SingleAlbum(id) ->
          single_album_view.render(
            library_if_loaded(model),
            id,
            model.settings,
            [
              attribute.id("single-album-view"),
              attribute.class("glass-bg"),
              start_play.on(StartPlay),
            ],
          )
      },
    ]),
    case model.play_status {
      HasTracks(PlayInfo(player: player, ..)) ->
        player.view(player)
        |> element.map(PlayerMsg)
      NoTracks -> text("")
    },
  ])
}

fn handle_start_play(model: Model, queue: PlayQueue, position: Int) {
  let assert Ok(#(track_id, track)) = list.at(queue, position)

  let #(player_model, maybe_init_effect) = case model.play_status {
    HasTracks(PlayInfo(player: p, ..)) -> #(p, effect.none())
    NoTracks -> {
      let assert option.Some(settings) = model.settings
      player.init(
        track_id,
        track,
        settings,
        model.request_config,
        model.library,
      )
    }
  }

  let #(new_model, e) =
    utils.update_child(
      player_model,
      player.StartPlay(track_id, track),
      player.update,
      PlayerMsg,
    )

  #(
    HasTracks(PlayInfo(
      track_id,
      track,
      queue,
      position,
      new_model,
      Tracking(0.0, 0.0),
    )),
    effect.batch([effect.map(maybe_init_effect, PlayerMsg), e]),
  )
}

fn if_player(
  model: Model,
  mapper: fn(PlayInfo) -> #(Model, effect.Effect(Msg)),
) -> #(Model, effect.Effect(Msg)) {
  case model.play_status {
    HasTracks(PlayInfo(..) as info) -> mapper(info)
    NoTracks -> #(model, effect.none())
  }
}

fn library_if_loaded(model: Model) {
  case model.loading_library {
    True -> option.None
    False -> option.Some(model.library)
  }
}

fn load_library(model: Model) {
  use dispatch <- effect.from()

  library_api.get_library(model.request_config, http.requestor())
  |> promise.map(LibraryResult)
  |> promise.tap(dispatch)

  Nil
}

fn form_request_config(auth_data: common.AuthData) {
  RequestConfig(
    auth_info: authed_request.AuthInfo(auth_data.user.id, auth_data.user.token),
    base_config: base_request_config(auth_data.device.name),
  )
}

fn update_play_info(play_info: PlayInfo, msg: player.Msg) -> PlayInfo {
  case msg {
    player.StartPlay(..) ->
      PlayInfo(..play_info, current_track_status: Tracking(0.0, 0.0))
    player.UpdateTime(time) -> {
      case play_info.current_track_status {
        Tracking(played, last_time) -> {
          let diff = time -. last_time

          // At maximum, only allow one second to be added, to account for the
          // user skipping forward
          let to_add = float.clamp(diff, min: 0.0, max: 1.0)
          let new_played = played +. to_add
          PlayInfo(
            ..play_info,
            current_track_status: Tracking(new_played, time),
          )
        }
        HistorySent -> play_info
      }
    }
    _ -> play_info
  }
}

fn maybe_send_history_status(play_info: PlayInfo, request_config: RequestConfig) {
  case play_info.current_track_status {
    Tracking(played, ..) -> {
      case played /. int.to_float(play_info.track.length) {
        percentage if percentage >=. elektrofoni.play_record_ratio -> {
          history.update_history(
            [
              history.HistoryEvent(
                track_id: play_info.track_id,
                track: play_info.track,
                type_: history.Play,
                when: birl.utc_now(),
              ),
            ],
            request_config,
            http.requestor(),
          )
          PlayInfo(..play_info, current_track_status: HistorySent)
        }
        _ -> play_info
      }
    }
    HistorySent -> play_info
  }
}

fn route_to_view(route: router.Route) {
  case route {
    router.TrackList -> Ok(library_view.Tracks)
    router.ArtistList -> Ok(library_view.Artists)
    router.AlbumList -> Ok(library_view.Albums)
    router.Artist(id) -> Ok(library_view.SingleArtist(id))
    router.Album(id) -> Ok(library_view.SingleAlbum(id))
    // TODO
    router.Settings -> Error(Nil)
  }
}

fn maybe_scroll(current_view: library_view.View, target_view: library_view.View) {
  event.on("click", fn(_) {
    case current_view == target_view {
      True -> Ok(LibraryViewScrollToTopRequested)
      False -> Error(Nil)
    }
  })
}
