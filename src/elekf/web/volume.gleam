import gleam/float
import ibroadcast/replay_gain

/// Calculate the gain to use for the audio pipeline's GainNode.
///
/// Returns a value from 0.0 to 1.0.
pub fn calculate(main_volume: Float, gain: Float) -> Float {
  let vol = main_volume *. replay_gain.factor(gain)
  float.min(vol, 1.0)
}
