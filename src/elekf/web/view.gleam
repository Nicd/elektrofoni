/// Views that can be rendered on the main level.
pub type View {
  LoginView
  AuthedView
}
