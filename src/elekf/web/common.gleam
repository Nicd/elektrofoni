import elekf/library/track.{type Track}
import elekf/api/auth/models as auth_models

/// A queue of tracks to play, with their IDs.
pub type PlayQueue =
  List(#(Int, Track))

/// Authentication data for the user.
pub type AuthData {
  AuthData(user: auth_models.User, device: auth_models.Device)
}

/// Settings of the iBroadcast API.
pub type Settings {
  Settings(artwork_server: String, streaming_server: String)
}
