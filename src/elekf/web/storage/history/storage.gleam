//// The history storage stores information about visited views for restoring
//// when the user next opens them.

import gleam/dynamic
import gleam/json
import gleam/dict
import gleam/list
import plinth/javascript/storage
import varasto
import elekf/web/storage/history/models

const storage_key = "__elektrofoni_history_storage"

const scrolls_key = "scroll-positions"

/// Storage format of the view history data in local storage.
pub type StorageFormat {
  StorageFormat(scrolls: models.ScrollPositions)
}

/// The local storage API used for storing view history details.
pub type HistoryStorage =
  varasto.TypedStorage(StorageFormat)

/// Gets the `varasto` instance to use for reading and writing auth storage.
pub fn get_api() {
  let assert Ok(local) = storage.local()
  varasto.new(local, reader(), writer())
}

/// Reads the previously stored value, if available.
pub fn read(storage: HistoryStorage) {
  varasto.get(storage, storage_key)
}

/// Writes new value to the storage.
pub fn write(storage: HistoryStorage, data: StorageFormat) {
  varasto.set(storage, storage_key, data)
}

/// Create an empty storage model.
pub fn new() {
  StorageFormat(scrolls: dict.new())
}

fn reader() {
  dynamic.decode1(StorageFormat, dynamic.field(scrolls_key, scrolls_decoder()))
}

fn writer() {
  fn(val: StorageFormat) {
    json.object([#(scrolls_key, scrolls_encoder(val.scrolls))])
  }
}

fn scrolls_encoder(scrolls: models.ScrollPositions) {
  scrolls
  |> dict.to_list()
  |> list.map(fn(item) {
    let #(key, val) = item
    #(key, json.float(val))
  })
  |> json.object()
}

fn scrolls_decoder() {
  dynamic.dict(dynamic.string, dynamic.float)
}
