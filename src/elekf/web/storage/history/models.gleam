import gleam/dict

pub type ScrollPositions =
  dict.Dict(String, Float)
