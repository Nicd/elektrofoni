import gleam/int
import gleam/float
import gleam/option
import lustre/element.{text}
import lustre/element/html.{div, p}
import lustre/attribute
import elekf/utils/lustre
import elekf/web/components/thumbnail
import elekf/web/components/player/model.{type Model}
import elekf/web/components/player/controls

pub fn view(model: Model) {
  div(
    [
      attribute.id("full-player-wrapper"),
      attribute.class("player-wrapper glass-bg glass-blur"),
    ],
    [
      thumbnail.maybe_view(
        option.Some(model.settings),
        "full-player-artwork",
        "full-player-thumbnail",
        "full-player-thumbnail full-player-thumbnail-placeholder",
        model.track.artwork_id,
        model.album.name,
      ),
      p([attribute.id("full-player-track-name")], [text(model.track.title)]),
      p([attribute.id("full-player-artist-name")], [text(model.artist.name)]),
      p([attribute.id("full-player-album-name")], [text(model.album.name)]),
      div([attribute.id("full-player-metas")], [
        meta("full-player-trackno", "Track", int.to_string(model.track.number)),
        meta("full-player-discno", "Disc", int.to_string(model.album.disc)),
        meta("full-player-genre", "Genre", model.track.genre),
        lustre.if_(model.track.year != 0, fn() {
          meta("full-player-year", "Year", int.to_string(model.track.year))
        }),
        meta(
          "full-player-replaygain",
          "ReplayGain",
          float.to_string(model.track.replay_gain),
        ),
        lustre.if_(model.track.rating > 0, fn() {
          meta(
            "full-player-rating",
            "Rating",
            int.to_string(model.track.rating) <> " / 5",
          )
        }),
        meta("full-player-plays", "Plays", int.to_string(model.track.plays)),
      ]),
      controls.view(model),
    ],
  )
}

fn meta(id: String, title: String, value: String) {
  div([attribute.id(id), attribute.class("full-player-meta")], [
    p([], [text(title)]),
    p([], [text(value)]),
  ])
}
