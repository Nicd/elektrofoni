import gleam/option
import lustre/element.{text}
import lustre/element/html.{div, p}
import lustre/attribute
import elekf/web/components/thumbnail
import elekf/web/components/player/model.{type Model}
import elekf/web/components/player/controls

pub fn view(model: Model) {
  div(
    [
      attribute.id("small-player-wrapper"),
      attribute.class(
        "player-wrapper glass-bg glass-shadow glass-blur glass-border",
      ),
    ],
    [
      thumbnail.maybe_view(
        option.Some(model.settings),
        "small-player-artwork",
        "small-player-thumbnail",
        "small-player-thumbnail small-player-thumbnail-placeholder",
        model.track.artwork_id,
        model.album.name,
      ),
      p([attribute.id("small-player-track-name")], [text(model.track.title)]),
      p([attribute.id("small-player-artist-name")], [text(model.artist.name)]),
      p([attribute.id("small-player-album-name")], [text(model.album.name)]),
      controls.view(model),
    ],
  )
}
