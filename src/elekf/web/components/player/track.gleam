//// A track for visualising the current song's progress and skipping it back
//// and forth, and additional timestamps.

import gleam/int
import gleam/dynamic
import lustre/element.{text}
import lustre/element/html.{input, p}
import lustre/attribute
import lustre/event
import elekf/web/components/track_length.{track_length}
import elekf/web/components/player/model
import elekf/web/components/player/actions.{
  Commit, EndUserSkip, Ephemeral, SelectPosition, StartUserSkip,
}

pub fn view(model: model.Model) {
  let current_time_padding = case model.track.length > 3600 {
    True -> track_length.Hours
    False -> track_length.Auto
  }

  let track_pos = dynamic.from(int.to_string(model.position))

  [
    p(
      [
        attribute.id("player-time-elapsed"),
        attribute.class("player-time"),
        attribute.attribute("aria-label", "Time elapsed"),
      ],
      [text(track_length(model.position, current_time_padding))],
    ),
    p(
      [
        attribute.id("player-time-total"),
        attribute.class("player-time"),
        attribute.attribute("aria-label", "Total time"),
      ],
      [text(track_length(model.track.length, track_length.Auto))],
    ),
    input([
      attribute.id("player-track"),
      attribute.type_("range"),
      attribute.min("0"),
      attribute.max(int.to_string(model.track.length)),
      attribute.value(track_pos),
      attribute.attribute("aria-label", "Track position"),
      event.on("input", input_handler),
      event.on("change", change_handler),
      event.on("mousedown", touch_start_handler),
      event.on("touchstart", touch_start_handler),
      event.on("mouseup", touch_end_handler),
      event.on("touchend", touch_end_handler),
    ]),
  ]
}

fn input_handler(_) {
  Ok(SelectPosition(Ephemeral))
}

fn change_handler(_) {
  Ok(SelectPosition(Commit))
}

fn touch_start_handler(_) {
  Ok(StartUserSkip)
}

fn touch_end_handler(_) {
  Ok(EndUserSkip)
}
