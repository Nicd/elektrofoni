pub type PositionSelection {
  Ephemeral
  Commit
}

pub type Action {
  NextTrack
  PrevTrack
  Play
  Pause
  Clear
  StartUserSkip
  EndUserSkip
  SelectPosition(type_: PositionSelection)
}
