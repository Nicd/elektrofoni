import lustre/element/html.{div}
import lustre/attribute
import lustre/event
import elekf/web/components/icon.{Alt, icon}
import elekf/web/components/button_group
import elekf/web/components/button
import elekf/web/components/link
import elekf/web/components/player/actions.{NextTrack, Pause, Play, PrevTrack}
import elekf/web/components/player/model.{type Model}
import elekf/web/components/player/track
import elekf/web/router

pub fn view(model: Model) {
  let is_playing = model.state == model.Playing
  let toggle_link = case model.view_mode {
    model.Small ->
      router.to_hash(router.current_with_query(router.fullscreen_player()))
    model.FullScreen -> "javascript:history.back()"
  }
  let toggle_icon = case model.view_mode {
    model.Small -> icon("arrows-angle-expand", Alt("Full screen player"))
    model.FullScreen -> icon("arrows-angle-contract", Alt("Small player"))
  }

  div([attribute.id("player-controls")], [
    button_group.view("", [attribute.id("player-controls-buttons")], [
      button.view(
        "",
        [attribute.id("player-previous"), event.on("click", on_prev_track)],
        [icon("skip-backward-fill", Alt("Previous"))],
      ),
      case is_playing {
        True ->
          button.view(
            "",
            [attribute.id("player-play-pause"), event.on("click", on_pause)],
            [icon("pause-fill", Alt("Pause"))],
          )
        False ->
          button.view(
            "",
            [attribute.id("player-play-pause"), event.on("click", on_play)],
            [icon("play-fill", Alt("Play"))],
          )
      },
      button.view(
        "",
        [attribute.id("player-next"), event.on("click", on_next_track)],
        [icon("skip-forward-fill", Alt("Next"))],
      ),
      link.button(toggle_link, "", [attribute.id("player-fullscreen-toggle")], [
        toggle_icon,
      ]),
    ]),
    ..track.view(model)
  ])
}

fn on_pause(_) {
  Ok(Pause)
}

fn on_play(_) {
  Ok(Play)
}

fn on_prev_track(_) {
  Ok(PrevTrack)
}

fn on_next_track(_) {
  Ok(NextTrack)
}
