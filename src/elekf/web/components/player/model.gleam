import gleam/uri
import gleam/option
import ibroadcast/authed_request.{type RequestConfig}
import elekf/web/common
import elekf/library.{type Library}
import elekf/library/track.{type Track}
import elekf/library/artist.{type Artist}
import elekf/library/album.{type Album}

pub type AudioContext

pub type MediaElementAudioSourceNode

pub type GainNode

pub type PlayState {
  Playing
  Paused
}

pub type ViewMode {
  Small
  FullScreen
}

pub type Model {
  Model(
    settings: common.Settings,
    library: Library,
    track_id: Int,
    track: Track,
    artist: Artist,
    album: Album,
    url: uri.Uri,
    position: Int,
    state: PlayState,
    loading_stream: Bool,
    request_config: RequestConfig,
    user_is_skipping: Bool,
    audio_context: AudioContext,
    audio_source: option.Option(MediaElementAudioSourceNode),
    gain_node: GainNode,
    view_mode: ViewMode,
  )
}
