import lustre/attribute
import lustre/element
import lustre/element/html.{div}

pub fn view(
  class: String,
  extra_attributes: List(attribute.Attribute(a)),
  content: List(element.Element(a)),
) {
  div([attribute.class("button-group " <> class), ..extra_attributes], content)
}
