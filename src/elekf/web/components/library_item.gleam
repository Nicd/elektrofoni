import lustre/attribute
import lustre/element
import lustre/element/html.{div}

/// An item in the library with its ID.
pub type LibraryItem(a) =
  #(Int, a)

pub fn view(
  class: String,
  extra_attributes: List(attribute.Attribute(a)),
  content: List(element.Element(a)),
) {
  div([attribute.class("library-item " <> class), ..extra_attributes], content)
}
