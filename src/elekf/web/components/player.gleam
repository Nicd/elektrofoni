//// The player view renders the media player UI element and its controls.

import gleam/uri
import gleam/int
import gleam/float
import gleam/option
import gleam/io
import lustre/element
import lustre/element/html.{audio, div}
import lustre/attribute
import lustre/event
import lustre/effect
import plinth/browser/media/metadata
import plinth/browser/media/session
import plinth/browser/media/action
import plinth/browser/media/position
import ibroadcast/authed_request.{type RequestConfig}
import ibroadcast/streaming
import ibroadcast/artwork
import elektrofoni
import elekf/utils/lustre
import elekf/web/common
import elekf/web/volume
import elekf/web/components/player/full_screen
import elekf/web/components/player/small
import elekf/web/components/player/model.{
  type AudioContext, type GainNode, type MediaElementAudioSourceNode, type Model,
  type PlayState, Model, Paused, Playing,
}
import elekf/web/components/player/actions
import elekf/library.{type Library}
import elekf/library/track.{type Track}
import elekf/library/artist.{type Artist}
import elekf/library/album.{type Album}
import elekf/utils/date

const default_seek_offset = 10.0

pub type Msg {
  StartPlay(Int, Track)
  PlayStateUpdated(PlayState)
  UpdateTime(Float)
  UpdateRequestConfig(RequestConfig)
  BackwardSeekRequested(Float)
  ForwardSeekRequested(Float)
  ComponentInitialised
  ActionTriggered(actions.Action)
  PositionSelected(Int)
  FullScreenOpened
  FullScreenClosed
}

pub fn init(
  track_id: Int,
  track: Track,
  settings: common.Settings,
  request_config: RequestConfig,
  library: Library,
) {
  let artist = library.assert_artist(library, track.artist_id)
  let album = library.assert_album(library, track.album_id)

  let action_effect =
    effect.from(fn(dispatch) {
      register_action_handler(
        action.NextTrack,
        ActionTriggered(actions.NextTrack),
        dispatch,
      )
      register_action_handler(
        action.PreviousTrack,
        ActionTriggered(actions.PrevTrack),
        dispatch,
      )
      register_action_handler(
        action.Play,
        ActionTriggered(actions.Play),
        dispatch,
      )
      register_action_handler(
        action.Pause,
        ActionTriggered(actions.Pause),
        dispatch,
      )
      session.set_action_handler(action.SeekBackward, fn(data) {
        let seek_amount = option.unwrap(data.seek_offset, default_seek_offset)
        dispatch(BackwardSeekRequested(seek_amount))
      })
      session.set_action_handler(action.SeekForward, fn(data) {
        let seek_amount = option.unwrap(data.seek_offset, default_seek_offset)
        dispatch(ForwardSeekRequested(seek_amount))
      })
      session.set_action_handler(action.SeekTo, fn(data) {
        let seek_to = option.unwrap(data.seek_time, 0.0)
        dispatch(PositionSelected(float.round(seek_to)))
      })
      lustre.after_next_render(fn() { dispatch(ComponentInitialised) })
    })

  start_play(settings, artist, album, track)

  let audio_context = create_audio_context()

  #(
    Model(
      settings,
      library,
      track_id,
      track,
      artist,
      album,
      form_url(track_id, track, settings, request_config),
      0,
      Playing,
      False,
      request_config,
      False,
      audio_context,
      option.None,
      create_gain(audio_context),
      model.Small,
    ),
    action_effect,
  )
}

pub fn update(model: Model, msg) {
  case msg {
    ActionTriggered(action) -> {
      case action {
        actions.Play -> {
          play()
          #(model, effect.none())
        }
        actions.Pause -> {
          pause()
          #(model, effect.none())
        }
        actions.NextTrack -> #(model, effect.none())
        actions.PrevTrack -> #(model, effect.none())
        actions.Clear -> #(model, effect.none())
        actions.StartUserSkip -> #(
          Model(..model, user_is_skipping: True),
          effect.none(),
        )
        actions.EndUserSkip -> #(
          Model(..model, user_is_skipping: False),
          effect.none(),
        )
        actions.SelectPosition(actions.Commit) -> {
          skip_to_time(model.position)

          #(model, effect.none())
        }
        actions.SelectPosition(actions.Ephemeral) -> {
          let pos = current_track_position()
          #(Model(..model, position: pos), effect.none())
        }
      }
    }
    StartPlay(track_id, track) -> {
      let artist = library.assert_artist(model.library, track.artist_id)
      let album = library.assert_album(model.library, track.album_id)
      start_play(model.settings, artist, album, track)

      linear_ramp_to_value(
        model.gain_node,
        volume.calculate(1.0, track.replay_gain),
        0.5,
      )

      let url = form_url(track_id, track, model.settings, model.request_config)
      #(
        Model(
          ..model,
          track_id: track_id,
          track: track,
          artist: artist,
          album: album,
          position: 0,
          url: url,
        ),
        effect.none(),
      )
    }
    PlayStateUpdated(play_state) -> #(
      Model(..model, state: play_state),
      effect.none(),
    )
    UpdateTime(time) ->
      case model.user_is_skipping {
        True -> #(model, effect.none())
        False -> {
          let pos = float.truncate(time)

          case
            position.make_position_state(
              position.Finite(int.to_float(model.track.length)),
              option.None,
              option.Some(time),
            )
          {
            Ok(state) -> session.set_position_state(state)
            Error(_) -> {
              io.println("Unable to set position state")
              io.debug(#(model.track.length, 1.0, time))
              Nil
            }
          }

          #(Model(..model, position: pos), effect.none())
        }
      }
    UpdateRequestConfig(config) -> #(
      Model(..model, request_config: config),
      effect.none(),
    )
    BackwardSeekRequested(amount) -> {
      let new_pos = float.round(current_time() -. amount)
      skip_to_time(new_pos)
      #(Model(..model, position: new_pos), effect.none())
    }
    ForwardSeekRequested(amount) -> {
      let new_pos = float.round(current_time() +. amount)
      skip_to_time(new_pos)
      #(Model(..model, position: new_pos), effect.none())
    }
    PositionSelected(pos) -> {
      skip_to_time(pos)
      #(Model(..model, position: pos), effect.none())
    }
    ComponentInitialised -> {
      let source = create_media_element_source(model.audio_context)
      connect_gain(model.gain_node, model.audio_context, source)

      #(Model(..model, audio_source: option.Some(source)), effect.none())
    }
    FullScreenOpened -> {
      #(Model(..model, view_mode: model.FullScreen), effect.none())
    }
    FullScreenClosed -> {
      #(Model(..model, view_mode: model.Small), effect.none())
    }
  }
}

pub fn view(model: Model) {
  let src = uri.to_string(model.url)

  div([attribute.id("player-wrapper")], [
    audio(
      [
        attribute.id("player-elem"),
        attribute.src(src),
        attribute.controls(False),
        attribute.autoplay(True),
        attribute.attribute("crossorigin", "anonymous"),
        event.on("play", event_play),
        event.on("pause", event_pause),
        event.on("ended", event_ended),
        event.on("timeupdate", event_timeupdate),
      ],
      [],
    ),
    case model.view_mode {
      model.Small ->
        small.view(model)
        |> element.map(ActionTriggered)
      model.FullScreen ->
        full_screen.view(model)
        |> element.map(ActionTriggered)
    },
  ])
}

fn form_url(
  track_id: Int,
  track: Track,
  settings: common.Settings,
  request_config: RequestConfig,
) {
  streaming.form_url(
    track.file,
    track_id,
    request_config,
    settings.streaming_server,
    elektrofoni.bitrate,
    date.unix_now() + elektrofoni.track_expiry_length,
  )
}

fn event_play(_: a) {
  Ok(PlayStateUpdated(Playing))
}

fn event_pause(_: a) {
  Ok(PlayStateUpdated(Paused))
}

fn event_ended(_: a) {
  Ok(ActionTriggered(actions.NextTrack))
}

fn event_timeupdate(_: a) {
  Ok(UpdateTime(current_time()))
}

fn start_play(
  settings: common.Settings,
  artist: Artist,
  album: Album,
  track: Track,
) {
  let metadata =
    metadata.new()
    |> metadata.set_title(track.title)
    |> metadata.set_artist(artist.name)
    |> metadata.set_album(album.name)

  let metadata = case track.artwork_id {
    option.Some(id) ->
      metadata.set_artwork(metadata, [
        metadata.MetadataArtwork(
          src: artwork.url(
            settings.artwork_server,
            int.to_string(id),
            artwork.S300,
          ),
          sizes: "300x300",
          type_: "",
        ),
      ])
    option.None -> metadata
  }

  session.set_metadata(metadata)
}

fn register_action_handler(
  action: action.Action,
  msg: Msg,
  dispatch: fn(Msg) -> Nil,
) {
  session.set_action_handler(action, fn(_) { dispatch(msg) })
}

@external(javascript, "../../../player_ffi.mjs", "play")
fn play() -> Nil

@external(javascript, "../../../player_ffi.mjs", "pause")
fn pause() -> Nil

@external(javascript, "../../../player_ffi.mjs", "currentTime")
fn current_time() -> Float

@external(javascript, "../../../player_ffi.mjs", "skipToPosition")
fn skip_to_time(time: Int) -> Nil

@external(javascript, "../../../player_ffi.mjs", "currentTrackPosition")
fn current_track_position() -> Int

@external(javascript, "../../../player_ffi.mjs", "createAudioContext")
fn create_audio_context() -> AudioContext

@external(javascript, "../../../player_ffi.mjs", "createMediaElementSource")
fn create_media_element_source(ctx: AudioContext) -> MediaElementAudioSourceNode

@external(javascript, "../../../player_ffi.mjs", "createGain")
fn create_gain(ctx: AudioContext) -> GainNode

@external(javascript, "../../../player_ffi.mjs", "connectGain")
fn connect_gain(
  node: GainNode,
  ctx: AudioContext,
  source: MediaElementAudioSourceNode,
) -> Nil

@external(javascript, "../../../player_ffi.mjs", "linearRampToValue")
fn linear_ramp_to_value(node: GainNode, gain: Float, at: Float) -> Nil
