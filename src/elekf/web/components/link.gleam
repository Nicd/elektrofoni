import lustre/attribute
import lustre/element
import lustre/element/html.{a}

pub fn button(
  href: String,
  class: String,
  extra_attributes: List(attribute.Attribute(a)),
  content: List(element.Element(a)),
) {
  a(
    [
      attribute.href(href),
      attribute.class("glass-button " <> class),
      ..extra_attributes
    ],
    content,
  )
}

pub fn link(
  href: String,
  class: String,
  extra_attributes: List(attribute.Attribute(a)),
  content: List(element.Element(a)),
) {
  a([attribute.href(href), attribute.class(class), ..extra_attributes], content)
}
