import lustre/attribute
import lustre/element
import lustre/element/html.{button}

pub fn view(
  class: String,
  extra_attributes: List(attribute.Attribute(a)),
  content: List(element.Element(a)),
) {
  button(
    [
      attribute.class("glass-button " <> class),
      attribute.type_("button"),
      ..extra_attributes
    ],
    content,
  )
}
