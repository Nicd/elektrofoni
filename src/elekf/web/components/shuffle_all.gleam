import lustre/element/html.{div}
import lustre/element.{text}
import lustre/attribute
import elekf/web/components/icon

pub fn view(extra_attrs: List(attribute.Attribute(a))) {
  div(
    [attribute.class("library-item library-list-shuffle-all"), ..extra_attrs],
    [icon.icon("shuffle", icon.Hidden), text(" Shuffle all")],
  )
}
