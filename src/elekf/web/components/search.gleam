//// The search view renders a search bar, other views must do the searching
//// based on the emitted messages.

import gleam/dynamic
import lustre/element/html.{div, input}
import lustre/attribute
import lustre/event
import elekf/web/components/icon.{Alt, icon}
import elekf/web/components/button
import elekf/utils/lustre

const search_bar_input_id = "search-bar-input"

type SearchElement

pub type Model {
  Model(search_text: String, show_search: Bool)
}

pub type Msg {
  ToggleShow
  UpdateSearch(String)
}

pub fn init() {
  Model(search_text: "", show_search: False)
}

pub fn update(model: Model, msg) {
  case msg {
    ToggleShow -> {
      let show_search = !model.show_search
      case show_search {
        True -> lustre.after_next_render(focus)
        False -> Nil
      }

      Model(..model, show_search: show_search)
    }
    UpdateSearch(text) -> Model(..model, search_text: text)
  }
}

pub fn view(model: Model) {
  div([attribute.id("search-bar")], [
    lustre.if_(model.show_search, fn() {
      div([attribute.id("search-bar-input-wrapper")], [
        input([
          attribute.id(search_bar_input_id),
          attribute.class("glass-input"),
          attribute.type_("search"),
          attribute.placeholder("Search"),
          attribute.attribute("aria-label", "Search through content"),
          attribute.value(dynamic.from(model.search_text)),
          event.on_input(UpdateSearch),
        ]),
      ])
    }),
    button.view(
      "button-small",
      [
        attribute.attribute("aria-controls", search_bar_input_id),
        event.on_click(ToggleShow),
      ],
      [icon("search", Alt("Toggle search"))],
    ),
  ])
}

fn focus() {
  get_elem(search_bar_input_id)
  |> do_focus()
}

@external(javascript, "../../../search_ffi.mjs", "focus")
fn do_focus(elem: SearchElement) -> Nil

@external(javascript, "../../../search_ffi.mjs", "getElem")
fn get_elem(id: String) -> SearchElement
