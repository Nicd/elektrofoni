//// A view to the library, presenting artists, albums, or tracks. This
//// component cannot be used directly, this should be extended by the actual
//// components that focus on a specific type of view to the library.

import gleam/dynamic
import gleam/list
import gleam/dict
import gleam/option
import gleam/string
import gleam/result
import gleam/int
import gleam/set
import lustre
import lustre/effect
import lustre/element.{type Element, text}
import lustre/element/html.{div, h1, p}
import lustre/attribute
import lustre/event
import elekf/utils/lustre as lustre_utils
import elekf/utils/order.{type Sorter}
import elekf/library.{type Library}
import elekf/library/track.{type Track}
import elekf/web/events/start_play
import elekf/web/events/scroll_to
import elekf/web/components/search
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/shuffle_all
import elekf/web/components/track_length
import elekf/web/common
import elekf/web/storage/history/storage as history_store

/// Function to get the data of the view from the library.
pub type DataGetter(a, filter) =
  fn(Library, filter) -> List(LibraryItem(a))

/// A view to render the header of the library view.
pub type HeaderView(a, filter) =
  fn(Model(a, filter), Library, filter, List(LibraryItem(a))) ->
    List(Element(Msg(filter)))

/// A view that renders a single item from the library.
///
/// It should return a list of elements, which can be used for expanding items:
/// the first element for the item itself and subsequent elements for the
/// expanded contents.
pub type ItemView(a, filter) =
  fn(Model(a, filter), Library, List(LibraryItem(a)), Int, LibraryItem(a)) ->
    List(Element(Msg(filter)))

/// A filter that might not yet be loaded. If it's not loaded, no data should be
/// fetched yet.
pub type Filter(filter) =
  option.Option(filter)

pub opaque type LibraryStatus {
  HaveLibrary(library: Library)
  NoLibrary
}

/// A filter that gets the item and the current search string and must return
/// `True` if the item should be shown and `False` if not.
///
/// The search string is lowercased.
pub type SearchFilter(a) =
  fn(a, String) -> Bool

/// Shuffler takes all of the items, finds all of the tracks in them, and
/// shuffles them for playback.
pub type Shuffler(a) =
  fn(Library, List(LibraryItem(a))) -> List(LibraryItem(Track))

pub type View {
  /// All albums.
  Albums
  /// Tracks of a single album.
  SingleAlbum(Int)
  /// All artists.
  Artists
  /// Albums of a single artist.
  SingleArtist(Int)
  /// Tracks of a single artist.
  /// SingleArtistTracks(Int, Artist)
  /// All tracks.
  Tracks
}

pub type Msg(filter) {
  LibraryUpdated(Library)
  SettingsUpdated(option.Option(common.Settings))
  ShuffleAll
  StartPlay(List(LibraryItem(Track)), Int)
  Search(search.Msg)
  FilterUpdated(filter)
  ListScrolled(Float)
  ScrollRequested(Float)
}

pub type Model(a, filter) {
  Model(
    id: String,
    filter: Filter(filter),
    library_status: LibraryStatus,
    data: List(LibraryItem(a)),
    data_getter: DataGetter(a, filter),
    shuffler: Shuffler(a),
    sorter: Sorter(a),
    search: search.Model,
    settings: option.Option(common.Settings),
    history: history_store.StorageFormat,
    history_api: history_store.HistoryStorage,
  )
}

/// Register the component as a custom element in the app.
///
/// This must be implemented by the client component, passing suitable values.
///
/// `data_getter` must be a callback that gets the library and must return the
/// items to show.
/// `item_view` is the renderer for individual items.
/// `shuffler` is the function used to find all the tracks in the current view
/// and shuffle them for play.
pub fn register(
  name: String,
  data_getter: DataGetter(a, filter),
  header_view: HeaderView(a, filter),
  item_view: ItemView(a, filter),
  shuffler: Shuffler(a),
  sorter: Sorter(a),
  filter: Filter(filter),
  search_filter: SearchFilter(a),
  extra_attrs: dict.Dict(String, dynamic.Decoder(Msg(filter))),
) {
  lustre.component(
    name,
    fn() { init(name, filter, data_getter, shuffler, sorter) },
    update,
    generate_view(header_view, item_view, search_filter),
    dict.merge(generic_attributes(), extra_attrs),
  )
}

/// Get the generic properties common to all library views, that can be input
/// into the Lustre component attribute change
pub fn generic_attributes() {
  dict.from_list([#("library", library_decode), #("settings", settings_decode)])
}

/// Render the component using a custom element.
pub fn render(
  name: String,
  library: option.Option(Library),
  settings: option.Option(common.Settings),
  extra_attrs: List(attribute.Attribute(msg)),
) {
  element.element(
    name,
    [
      attribute.property("library", library),
      attribute.property("settings", settings),
      ..extra_attrs
    ],
    [],
  )
}

@external(javascript, "../../../library_view_ffi.mjs", "requestScroll")
pub fn request_scroll(pos: Float) -> Nil

pub fn init(id, filter, data_getter, shuffler, sorter) {
  let scrollend_effect =
    effect.from(fn(dispatch) {
      lustre_utils.after_next_render(fn() {
        add_scrollend_listener(fn(pos) { dispatch(ListScrolled(pos)) })
      })
    })

  let history_api = history_store.get_api()
  let history = get_view_history(history_api)

  let scroll_to = dict.get(history.scrolls, id)
  let scroll_to_effect = case scroll_to {
    Ok(pos) if pos >. 0.0 ->
      effect.from(fn(dispatch) {
        lustre_utils.after_next_render(fn() { dispatch(ScrollRequested(pos)) })
      })
    _ -> effect.none()
  }

  #(
    Model(
      id,
      filter,
      NoLibrary,
      [],
      data_getter,
      shuffler,
      sorter,
      search.init(),
      option.None,
      history,
      history_api,
    ),
    effect.batch([scrollend_effect, scroll_to_effect]),
  )
}

pub fn update(model: Model(a, filter), msg) {
  case msg {
    LibraryUpdated(library) -> #(
      update_data(Model(..model, library_status: new_library(library))),
      effect.none(),
    )
    SettingsUpdated(settings) -> #(
      Model(..model, settings: settings),
      effect.none(),
    )
    ShuffleAll -> #(model, shuffle_all(model))
    StartPlay(tracks, position) -> #(model, start_play.emit(tracks, position))
    Search(search_msg) -> {
      let search_model = search.update(model.search, search_msg)
      #(Model(..model, search: search_model), effect.none())
    }

    FilterUpdated(filter) -> {
      let new_model = Model(..model, filter: option.Some(filter))
      #(update_data(new_model), effect.none())
    }

    ListScrolled(pos) -> {
      let view_history = get_view_history(model.history_api)
      let updated_history =
        history_store.StorageFormat(scrolls: dict.insert(
          view_history.scrolls,
          model.id,
          pos,
        ))
      let _ = history_store.write(model.history_api, updated_history)
      #(model, effect.none())
    }

    ScrollRequested(pos) -> {
      scroll_to(pos)
      #(model, effect.none())
    }
  }
}

pub fn library_view(
  model: Model(a, filter),
  header_view: HeaderView(a, filter),
  item_view: ItemView(a, filter),
  search_filter: SearchFilter(a),
) {
  case model.library_status, model.filter {
    HaveLibrary(lib), option.Some(f) -> {
      let items = case model.search.search_text {
        "" -> model.data
        txt -> {
          let search_txt = string.lowercase(txt)
          model.data
          |> list.filter(fn(item) { search_filter(item.1, search_txt) })
        }
      }

      div(
        [attribute.id("library-list"), scroll_to.on(ScrollRequested)],
        list.append(
          [
            search.view(model.search)
              |> element.map(Search),
            div(
              [attribute.id("library-list-header")],
              header_view(model, lib, f, items),
            ),
            shuffle_all.view([event.on_click(ShuffleAll)]),
          ],
          list.index_map(items, fn(item, i) {
              item_view(model, lib, items, i, item)
            })
            |> list.flatten(),
        ),
      )
    }
    _, _ ->
      div(
        [attribute.id("library-list"), attribute.class("library-list-loading")],
        [text("")],
      )
  }
}

/// An empty header view that renders nothing.
pub fn empty_header(_model, _library, _filter, _items) {
  []
}

/// A header that renders some statistics about the found tracks.
pub fn stats_header(
  title: String,
  _model,
  _library,
  _filter,
  items: List(LibraryItem(Track)),
) {
  let #(tracks, seconds, artists, albums) =
    list.fold(items, #(0, 0, set.new(), set.new()), fn(acc, item) {
      let #(_i, track) = item
      let #(tracks, seconds, artists, albums) = acc
      #(
        tracks + 1,
        seconds + track.length,
        set.insert(artists, track.artist_id),
        set.insert(albums, track.album_id),
      )
    })
  let duration =
    track_length.humanize_length(
      seconds,
      track_length.Auto,
      track_length.short_delimiters,
    )

  [
    div([attribute.id("library-list-stats-header")], [
      h1([], [text(title)]),
      p([], [
        text(
          int.to_string(tracks)
          <> " tracks by "
          <> int.to_string(set.size(artists))
          <> " artists in "
          <> int.to_string(set.size(albums))
          <> " albums, "
          <> duration,
        ),
      ]),
    ]),
  ]
}

fn generate_view(
  header_view: HeaderView(a, filter),
  item_view: ItemView(a, filter),
  search_filter: SearchFilter(a),
) {
  library_view(_, header_view, item_view, search_filter)
}

fn shuffle_all(model: Model(a, filter)) {
  case model.library_status {
    HaveLibrary(lib) -> {
      let tracks = model.shuffler(lib, model.data)
      start_play.emit(tracks, 0)
    }
    NoLibrary -> effect.none()
  }
}

fn library_decode(data: dynamic.Dynamic) {
  let library: option.Option(Library) = dynamic.unsafe_coerce(data)
  case library {
    option.Some(lib) -> Ok(LibraryUpdated(lib))
    option.None -> Error([])
  }
}

fn settings_decode(data: dynamic.Dynamic) {
  let settings: option.Option(common.Settings) = dynamic.unsafe_coerce(data)
  Ok(SettingsUpdated(settings))
}

fn update_data(model: Model(a, filter)) {
  case model.library_status, model.filter {
    HaveLibrary(l), option.Some(f) ->
      Model(
        ..model,
        data: l
        |> model.data_getter(f)
        |> list.sort(fn(a, b) { model.sorter(a.1, b.1) }),
      )
    _, _ -> model
  }
}

fn get_view_history(history_api) {
  result.unwrap(history_store.read(history_api), history_store.new())
}

@external(javascript, "../../../library_view_ffi.mjs", "addScrollendListener")
fn add_scrollend_listener(callback: fn(Float) -> Nil) -> Nil

@external(javascript, "../../../library_view_ffi.mjs", "scrollTo")
fn scroll_to(pos: Float) -> Nil

fn new_library(library: Library) -> LibraryStatus {
  HaveLibrary(library)
}
