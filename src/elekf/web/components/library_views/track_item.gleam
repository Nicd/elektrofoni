import gleam/int
import lustre/attribute
import lustre/event
import lustre/element.{text}
import lustre/element/html.{h3, p}
import elekf/library.{type Library}
import elekf/library/track.{type Track}
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/library_view.{StartPlay}

pub fn view(
  library: Library,
  items: List(LibraryItem(Track)),
  index: Int,
  item: LibraryItem(Track),
  id_prefix: String,
) {
  let #(track_id, track) = item
  let album = library.assert_album(library, track.album_id)
  let artist = library.assert_artist(library, track.artist_id)
  [
    library_item.view(
      "track-item",
      [
        attribute.id(id_prefix <> "-" <> int.to_string(track_id)),
        attribute.type_("button"),
        event.on_click(StartPlay(items, index)),
        attribute.attribute("role", "button"),
      ],
      [
        h3([attribute.class("track-title")], [text(track.title)]),
        p([attribute.class("track-artist")], [text(artist.name)]),
        p([attribute.class("track-album")], [text(album.name)]),
      ],
    ),
  ]
}
