//// A library view to all of the artists in the library.

import gleam/string
import gleam/int
import gleam/list
import gleam/dict
import gleam/option
import lustre/element/html.{h3, p}
import lustre/element.{text}
import lustre/attribute
import elekf/library.{type Library}
import elekf/library/artist.{type Artist}
import elekf/library/artist_utils
import elekf/web/components/library_view.{type Model}
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/thumbnail
import elekf/web/common
import elekf/web/router
import elekf/web/components/link

const component_name = "artists-view"

/// Register the artists view as a custom element.
pub fn register() {
  library_view.register(
    component_name,
    data_getter,
    header_view,
    item_view,
    shuffler,
    artist_utils.sort_by_name,
    option.Some(Nil),
    search_filter,
    dict.new(),
  )
}

/// Render the artists view.
pub fn render(
  library: option.Option(Library),
  settings: option.Option(common.Settings),
  extra_attrs: List(attribute.Attribute(msg)),
) {
  library_view.render(component_name, library, settings, extra_attrs)
}

fn data_getter(library: Library, _filter: Nil) {
  library.artists
  |> dict.fold([], fn(acc, key, val) {
    case val.tracks {
      [] -> acc
      _ -> [#(key, val), ..acc]
    }
  })
}

fn shuffler(library, items: List(LibraryItem(Artist))) {
  items
  |> list.shuffle()
  |> list.map(fn(artist) { get_tracks(library, artist.1) })
  |> list.flatten()
}

fn search_filter(item: Artist, search_text: String) {
  string.contains(item.name_lower, search_text)
}

fn header_view(
  model: Model(Artist, Nil),
  library: Library,
  filter: Nil,
  items: List(LibraryItem(Artist)),
) {
  let tracks =
    list.fold(items, [], fn(acc, artist) {
      list.concat([acc, get_tracks(library, artist.1)])
    })

  library_view.stats_header("Artists", model, library, filter, tracks)
}

fn item_view(
  model: Model(Artist, Nil),
  _library: Library,
  _items: List(LibraryItem(Artist)),
  _index: Int,
  item: LibraryItem(Artist),
) {
  let #(artist_id, artist) = item
  let artist_id_str = int.to_string(artist_id)
  [
    link.link(
      router.to_hash(router.queryless(router.Artist(artist_id))),
      "library-item artist-item",
      [attribute.id("artist-list-" <> artist_id_str)],
      [
        thumbnail.maybe_item_thumbnail(
          model.settings,
          "artist-list-thumbnail-" <> artist_id_str,
          artist.artwork_id,
          artist.name,
        ),
        h3([attribute.class("artist-title")], [text(artist.name)]),
        p([attribute.class("artist-tracks")], [
          text(int.to_string(list.length(artist.tracks)) <> " tracks"),
        ]),
      ],
    ),
  ]
}

fn get_tracks(library: Library, artist: Artist) {
  list.map(artist.tracks, fn(track_id) {
    #(track_id, library.assert_track(library, track_id))
  })
}
