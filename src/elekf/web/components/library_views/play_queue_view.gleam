//// A library view to the current play queue.

import gleam/string
import gleam/list
import gleam/dict
import gleam/option
import lustre/attribute
import elekf/library.{type Library}
import elekf/library/track.{type Track}
import elekf/utils/order
import elekf/web/components/library_view.{type Model} as library_view
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/library_views/track_item
import elekf/web/common

const component_name = "play-queue-view"

/// Register the play queue view as a custom element.
pub fn register() {
  library_view.register(
    component_name,
    data_getter,
    library_view.empty_header,
    item_view,
    shuffler,
    order.noop,
    option.None,
    search_filter,
    dict.new(),
  )
}

/// Render the play queue view.
pub fn render(
  library: option.Option(Library),
  settings: option.Option(common.Settings),
  extra_attrs: List(attribute.Attribute(msg)),
) {
  library_view.render(component_name, library, settings, extra_attrs)
}

fn data_getter(library: Library, _filter: Nil) {
  dict.to_list(library.tracks)
}

fn shuffler(_library, items) {
  list.shuffle(items)
}

fn search_filter(item: Track, search_text: String) {
  string.contains(item.title_lower, search_text)
}

fn item_view(
  _model: Model(Track, Nil),
  library: Library,
  items: List(LibraryItem(Track)),
  index: Int,
  item: LibraryItem(Track),
) {
  track_item.view(library, items, index, item, "track-list")
}
