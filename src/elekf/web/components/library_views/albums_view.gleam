//// A library view to all of the albums in the library.

import gleam/string
import gleam/list
import gleam/dict
import gleam/option
import lustre/attribute
import elekf/library.{type Library}
import elekf/library/album.{type Album}
import elekf/library/album_utils
import elekf/web/components/library_view.{type Model}
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/library_views/album_item
import elekf/web/common

const component_name = "albums-view"

/// Register the albums view as a custom element.
pub fn register() {
  library_view.register(
    component_name,
    data_getter,
    header_view,
    item_view,
    shuffler,
    album_utils.sort_by_name,
    option.Some(Nil),
    search_filter,
    dict.new(),
  )
}

/// Render the albums view.
pub fn render(
  library: option.Option(Library),
  settings: option.Option(common.Settings),
  extra_attrs: List(attribute.Attribute(msg)),
) {
  library_view.render(component_name, library, settings, extra_attrs)
}

fn data_getter(library: Library, _filter: Nil) {
  dict.to_list(library.albums)
}

fn shuffler(library, items: List(LibraryItem(Album))) {
  items
  |> list.shuffle()
  |> list.map(fn(album) { album_utils.get_tracks(library, album.1) })
  |> list.flatten()
}

fn search_filter(item: Album, search_text: String) {
  string.contains(item.name_lower, search_text)
}

fn header_view(
  model: Model(Album, Nil),
  library: Library,
  filter: Nil,
  items: List(LibraryItem(Album)),
) {
  let tracks =
    list.fold(items, [], fn(acc, artist) {
      list.concat([acc, album_utils.get_tracks(library, artist.1)])
    })

  library_view.stats_header("Albums", model, library, filter, tracks)
}

fn item_view(
  model: Model(Album, Nil),
  library: Library,
  _items: List(LibraryItem(Album)),
  _index: Int,
  item: LibraryItem(Album),
) {
  album_item.view(library, model.settings, item)
}
