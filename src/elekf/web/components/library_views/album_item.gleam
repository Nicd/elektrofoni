//// Library item view for a single album.

import gleam/list
import gleam/option
import gleam/int
import lustre/element/html.{h3, p}
import lustre/element.{text}
import lustre/attribute
import elekf/library.{type Library}
import elekf/library/album.{type Album}
import elekf/library/album_utils
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/thumbnail
import elekf/web/components/link
import elekf/web/common.{type Settings}
import elekf/web/router

pub fn view(
  library: Library,
  settings: option.Option(Settings),
  item: LibraryItem(Album),
) {
  let #(album_id, album) = item
  let album_id_str = int.to_string(album_id)
  let tracks = album_utils.get_tracks(library, album)
  let artist_name = case album.artist_id {
    0 -> "Unknown artist"
    id -> library.assert_artist(library, id).name
  }
  let assert Ok(first_track) = list.first(tracks)

  [
    link.link(
      router.to_hash(router.queryless(router.Album(album_id))),
      "library-item album-item",
      [attribute.id("album-list-" <> album_id_str)],
      [
        thumbnail.maybe_item_thumbnail(
          settings,
          "album-item-thumbnail-" <> album_id_str,
          { first_track.1 }.artwork_id,
          album.name,
        ),
        h3([attribute.class("album-item-title")], [text(album.name)]),
        p([attribute.class("album-item-artist")], [text(artist_name)]),
        p([attribute.class("album-item-tracks-meta")], [
          text(int.to_string(list.length(album.tracks)) <> " tracks"),
        ]),
      ],
    ),
  ]
}
