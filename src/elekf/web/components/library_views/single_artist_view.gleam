//// A library view to a single artist's albums and non-album tracks.

import gleam/string
import gleam/list
import gleam/dict
import gleam/option
import gleam/dynamic
import gleam/set
import gleam/int
import lustre/attribute
import lustre/element.{text}
import lustre/element/html.{div, h1, p}
import elekf/library.{type Library}
import elekf/library/track.{type Track}
import elekf/library/track_utils
import elekf/web/components/library_view
import elekf/web/components/library_item.{type LibraryItem}
import elekf/web/components/library_views/track_item
import elekf/web/common
import elekf/web/components/track_length

const component_name = "single-artist-view"

/// Register the single artist view as a custom element.
pub fn register() {
  library_view.register(
    component_name,
    data_getter,
    header_view,
    item_view,
    shuffler,
    track_utils.sort_by_name,
    option.None,
    search_filter,
    dict.from_list([#("artist-id", id_decode)]),
  )
}

/// Render the single artist view.
pub fn render(
  library: option.Option(Library),
  artist_id: Int,
  settings: option.Option(common.Settings),
  extra_attrs: List(attribute.Attribute(msg)),
) {
  library_view.render(component_name, library, settings, [
    attribute.property("artist-id", artist_id),
    ..extra_attrs
  ])
}

fn data_getter(library: Library, filter: Int) {
  let artist = library.assert_artist(library, filter)
  list.map(artist.tracks, fn(track_id) {
    #(track_id, library.assert_track(library, track_id))
  })
}

fn shuffler(_library, items: List(LibraryItem(Track))) {
  items
  |> list.shuffle()
}

fn search_filter(item: Track, search_text: String) {
  string.contains(item.title_lower, search_text)
}

fn header_view(
  model: library_view.Model(Track, Int),
  library: Library,
  filter: Int,
  items: List(LibraryItem(Track)),
) {
  case library.get_artist(library, filter) {
    Ok(artist) -> {
      let #(tracks, seconds, albums) =
        list.fold(items, #(0, 0, set.new()), fn(acc, item) {
          let #(_i, track) = item
          let #(tracks, seconds, albums) = acc
          #(
            tracks + 1,
            seconds + track.length,
            set.insert(albums, track.album_id),
          )
        })
      let duration =
        track_length.humanize_length(
          seconds,
          track_length.Auto,
          track_length.short_delimiters,
        )

      [
        div([attribute.id("library-list-stats-header")], [
          h1([], [text(artist.name)]),
          p([], [
            text(
              int.to_string(tracks)
              <> " tracks in "
              <> int.to_string(set.size(albums))
              <> " albums, "
              <> duration,
            ),
          ]),
        ]),
      ]
    }
    Error(_) -> library_view.empty_header(model, library, filter, items)
  }
}

fn item_view(
  _model: library_view.Model(Track, Int),
  library: Library,
  items: List(LibraryItem(Track)),
  index: Int,
  item: LibraryItem(Track),
) {
  track_item.view(library, items, index, item, "artist-tracks-list")
}

fn id_decode(data: dynamic.Dynamic) {
  let artist_id: Int = dynamic.unsafe_coerce(data)
  Ok(library_view.FilterUpdated(artist_id))
}
