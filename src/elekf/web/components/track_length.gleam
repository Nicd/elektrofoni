import gleam/int
import gleam/string

/// Use to select padding mode.
pub type PadTo {
  /// Show hours if track is an hour or longer.
  Auto
  /// Always show hours.
  Hours
}

/// Delimiters to use when humanizing a track length.
pub type Delimiters {
  Delimiters(hours: String, minutes: String, seconds: String)
}

/// Colons delimiting the duration, in the form of `1:23:45`.
pub const colon_delimiters = Delimiters(hours: ":", minutes: ":", seconds: "")

/// Short units delimiting the duration, in the form of `1 h 23 min 45 s.`.
pub const short_delimiters = Delimiters(
  hours: " h ",
  minutes: " min ",
  seconds: " s",
)

/// Get a track length (in seconds) as a formatted time duration with colon
/// delimiters.
pub fn track_length(length: Int, padding: PadTo) {
  humanize_length(length, padding, colon_delimiters)
}

/// Get a track length (in seconds) as a formatted time duration.
pub fn humanize_length(
  length: Int,
  padding: PadTo,
  delimiters: Delimiters,
) -> String {
  case length {
    l if l < 60 -> {
      case padding {
        Auto ->
          "00" <> delimiters.minutes <> pad_00(length) <> delimiters.seconds
        Hours ->
          "0"
          <> delimiters.hours
          <> "00"
          <> delimiters.minutes
          <> pad_00(length)
          <> delimiters.seconds
      }
    }
    l if l < 3600 -> {
      let mins = l / 60
      let secs = l - mins * 60

      let prefix = case padding {
        Auto -> ""
        Hours -> "0" <> delimiters.hours
      }

      prefix
      <> pad_00(mins)
      <> delimiters.minutes
      <> pad_00(secs)
      <> delimiters.seconds
    }
    l -> {
      let hours = l / 3600
      let hours_secs = hours * 3600
      let mins = { l - hours_secs } / 60
      let secs = l - hours_secs - mins * 60
      int.to_string(hours)
      <> delimiters.hours
      <> pad_00(mins)
      <> delimiters.minutes
      <> pad_00(secs)
      <> delimiters.seconds
    }
  }
}

fn pad_00(amount: Int) -> String {
  amount
  |> int.to_string()
  |> string.pad_left(2, "0")
}
