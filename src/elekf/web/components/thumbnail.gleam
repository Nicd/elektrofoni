//// A thumbnail element (artist, album...)

import gleam/option
import gleam/int
import lustre/element/html.{div, img}
import lustre/element.{text}
import lustre/attribute
import ibroadcast/artwork
import elekf/web/common.{type Settings}

pub fn view(s: Settings, image_id: String, html_id: String, class: String) {
  div(
    [
      attribute.id(html_id <> "-wrapper"),
      attribute.class("thumbnail " <> class),
    ],
    [
      img([
        attribute.id(html_id),
        attribute.attribute("aria-hidden", "true"),
        attribute.src(artwork.url(s.artwork_server, image_id, artwork.S300)),
        attribute.attribute("loading", "lazy"),
        attribute.width(300),
      ]),
    ],
  )
}

pub fn maybe_item_thumbnail(settings, id, artwork, placeholder) {
  maybe_view(
    settings,
    id,
    "library-item-thumbnail",
    "library-item-thumbnail library-item-thumbnail-placeholder",
    artwork,
    placeholder,
  )
}

pub fn maybe_view(
  settings: option.Option(Settings),
  id: String,
  class: String,
  placeholder_class: String,
  artwork: option.Option(Int),
  placeholder: String,
) {
  case settings, artwork {
    option.Some(s), option.Some(image_id) ->
      view(s, int.to_string(image_id), id, class)
    _, _ ->
      div([attribute.class("thumbnail-placeholder " <> placeholder_class)], [
        text(placeholder),
      ])
  }
}
