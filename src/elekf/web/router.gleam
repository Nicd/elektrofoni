import gleam/int
import gleam/string
import gleam/result
import gleam/set
import gleam/uri
import gleam/list
import plinth/browser/window

const artists = "artists"

const albums = "albums"

const settings = "settings"

const fullscreen_query = "fullscreen"

pub const default_route = TrackList

pub type Option {
  FullScreen
}

pub type Query =
  set.Set(Option)

pub type Route {
  TrackList
  ArtistList
  AlbumList
  Artist(id: Int)
  Album(id: Int)
  Settings
}

pub type RouteWithQuery {
  RouteWithQuery(route: Route, query: Query)
}

pub type Msg {
  RouteChanged(route: RouteWithQuery)
}

pub fn init(dispatch: fn(Msg) -> Nil) {
  window.add_event_listener("hashchange", fn(_e) {
    let _ = run(dispatch)
    Nil
  })
}

pub fn run(dispatch: fn(Msg) -> Nil) {
  let #(hash, query) = get_current_path()
  let query = parse_query(query)
  use route <- result.try(parse(hash))
  Ok(dispatch(RouteChanged(RouteWithQuery(route, query))))
}

pub fn to_hash(route_with_query: RouteWithQuery) {
  "#" <> to_string(route_with_query)
}

pub fn parse(route: String) {
  let parts =
    route
    |> string.drop_left(1)
    |> string.split("/")

  case parts {
    [""] -> Ok(TrackList)
    [a] if a == artists -> Ok(ArtistList)
    [a] if a == albums -> Ok(AlbumList)
    [a, id] -> {
      case a, int.parse(id) {
        a, Ok(id) if a == artists -> Ok(Artist(id))
        a, Ok(id) if a == albums -> Ok(Album(id))
        _, _ -> Error(Nil)
      }
    }
    [s] if s == settings -> Ok(Settings)

    _ -> Error(Nil)
  }
}

pub fn queryless(route: Route) {
  RouteWithQuery(route, set.new())
}

pub fn current_with_query(query: Query) {
  let #(path, _) = get_current_path()
  let route = result.unwrap(parse(path), default_route)
  RouteWithQuery(route, query)
}

pub fn fullscreen_player() {
  set.new()
  |> set.insert(FullScreen)
}

pub fn parse_query(query: List(#(String, String))) {
  list.fold(query, set.new(), fn(acc, item) {
    let #(key, _val) = item
    case key {
      q if q == fullscreen_query -> set.insert(acc, FullScreen)
      _ -> acc
    }
  })
}

pub fn get_current_path() {
  let hash = result.unwrap(window.get_hash(), "/")
  let #(path, query) = result.unwrap(string.split_once(hash, "?"), #(hash, ""))
  let query = result.unwrap(uri.parse_query(query), [])
  #(path, query)
}

fn to_string(route_with_query: RouteWithQuery) {
  let parts = case route_with_query.route {
    TrackList -> []
    ArtistList -> [artists]
    AlbumList -> [albums]
    Artist(id) -> [artists, int.to_string(id)]
    Album(id) -> [albums, int.to_string(id)]
    Settings -> [settings]
  }
  let query_str = query_to_string(route_with_query.query)

  "/" <> string.join(parts, "/") <> "?" <> query_str
}

fn query_to_string(query: Query) {
  let query_parts =
    set.fold(query, [], fn(acc, item) {
      case item {
        FullScreen -> [#(fullscreen_query, ""), ..acc]
      }
    })

  uri.query_to_string(query_parts)
}
