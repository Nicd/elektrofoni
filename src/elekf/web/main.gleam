//// The main view sets up everything and shows the login or authed view.

import gleam/io
import gleam/option
import lustre
import lustre/effect
import lustre/element/html.{div}
import lustre/element
import lustre/attribute
import elekf/web/common
import elekf/web/login_view
import elekf/web/authed_view
import elekf/web/view
import elekf/web/utils
import elekf/web/components/library_views/tracks_view
import elekf/web/components/library_views/artists_view
import elekf/web/components/library_views/albums_view
import elekf/web/components/library_views/single_artist_view
import elekf/web/components/library_views/single_album_view
import elekf/api/auth/storage as auth_storage
import elekf/api/auth/models as auth_models
import elekf/utils/option as option_utils
import ibroadcast/auth/login_token

type Model {
  Model(
    auth_storage: auth_storage.AuthStorage,
    auth_data: option.Option(common.AuthData),
    current_view: view.View,
    login_view: login_view.Model,
    authed_view: option.Option(authed_view.Model),
  )
}

type Msg {
  LoginView(login_view.Msg)
  AuthedView(authed_view.Msg)
}

pub fn main() {
  let assert Ok(_) = tracks_view.register()
  let assert Ok(_) = artists_view.register()
  let assert Ok(_) = albums_view.register()
  let assert Ok(_) = single_artist_view.register()
  let assert Ok(_) = single_album_view.register()

  let app = lustre.application(init, update, view)
  let assert Ok(_) = lustre.start(app, "[data-lustre-app]", Nil)

  Nil
}

fn init(_) {
  let auth_storage = auth_storage.get()

  let stored_auth_data = auth_storage.read(auth_storage)
  let auth_data = case stored_auth_data {
    Ok(storage_format) ->
      option.Some(common.AuthData(
        user: auth_models.User(
          id: storage_format.user.id,
          token: storage_format.user.token,
          session_uuid: storage_format.user.session_uuid,
        ),
        device: auth_models.Device(name: storage_format.device.name),
      ))
    Error(_) -> option.None
  }

  let current_view = case auth_data {
    option.Some(_) -> view.AuthedView
    option.None -> view.LoginView
  }

  let #(authed_view_model, authed_view_effect) = case auth_data {
    option.Some(data) -> {
      let #(m, e) = authed_view.init(data)
      #(option.Some(m), effect.map(e, AuthedView))
    }
    option.None -> #(option.None, effect.none())
  }

  #(
    Model(
      auth_storage: auth_storage,
      auth_data: auth_data,
      current_view: current_view,
      login_view: login_view.init(),
      authed_view: authed_view_model,
    ),
    authed_view_effect,
  )
}

fn update(model: Model, msg) {
  case msg {
    LoginView(login_view.LoginResult(Ok(login_token.AuthData(data)))) -> {
      logged_in(model, data.token, data.session)
    }
    LoginView(login_msg) -> {
      let #(login_model, login_effect) =
        utils.update_child(
          model.login_view,
          login_msg,
          login_view.update,
          LoginView,
        )
      #(Model(..model, login_view: login_model), login_effect)
    }
    AuthedView(authed_msg) -> {
      case model.authed_view {
        option.Some(authed_view) -> {
          let #(authed_model, authed_effect) =
            utils.update_child(
              authed_view,
              authed_msg,
              authed_view.update,
              AuthedView,
            )
          #(
            Model(..model, authed_view: option.Some(authed_model)),
            authed_effect,
          )
        }

        option.None -> #(model, effect.none())
      }
    }
  }
}

fn view(model: Model) {
  html.main([], [
    case model.current_view {
      view.LoginView ->
        div([attribute.id("login-view")], [
          login_view.view(model.login_view)
          |> element.map(LoginView),
        ])
      view.AuthedView ->
        div([attribute.id("authed-view")], [
          authed_view.view(option_utils.assert_some(model.authed_view))
          |> element.map(AuthedView),
        ])
    },
  ])
}

fn logged_in(model: Model, token: String, session: login_token.Session) {
  let auth_data =
    common.AuthData(
      user: auth_models.User(
        id: session.user_id,
        token: token,
        session_uuid: session.session_uuid,
      ),
      device: auth_models.Device(name: model.login_view.device_name),
    )

  let #(m, e) = authed_view.init(auth_data)

  case
    auth_storage.write(
      model.auth_storage,
      auth_storage.StorageFormat(user: auth_data.user, device: auth_data.device),
    )
  {
    Ok(_) -> Nil
    Error(err) -> {
      io.println_error("Unable to store authentication credentials:")
      io.debug(err)
    }
  }

  #(
    Model(
      ..model,
      auth_data: option.Some(auth_data),
      current_view: view.AuthedView,
      authed_view: option.Some(m),
    ),
    effect.map(e, AuthedView),
  )
}
