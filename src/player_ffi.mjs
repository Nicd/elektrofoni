const player_id = "player-elem";
const track_id = "player-track";

/**
 * @type {HTMLAudioElement}
 */
let player;

/**
 * @type {HTMLInputElement}
 */
let track;

export function registerCallback(event, callback) {
  getPlayer();
  player.addEventListener(event, callback);
}

export function currentTime() {
  getPlayer();
  return player.currentTime;
}

export function play() {
  getPlayer();
  player.play();
}

export function pause() {
  getPlayer();
  player.pause();
}

export function skipToPosition(pos) {
  getPlayer();
  player.currentTime = pos;
}

export function currentTrackPosition() {
  getTrack();
  return track.valueAsNumber;
}

export function createAudioContext() {
  return new globalThis.AudioContext();
}

export function createMediaElementSource(ctx) {
  getPlayer();
  return ctx.createMediaElementSource(player);
}

export function createGain(ctx) {
  return ctx.createGain();
}

export function connectGain(node, ctx, source) {
  source.connect(node);
  node.connect(ctx.destination);
}

export function linearRampToValue(node, gain, at) {
  node.gain.linearRampToValueAtTime(gain, at);
}

function getPlayer() {
  if (player === undefined) {
    player = document.getElementById(player_id);
  }
}

function getTrack() {
  // Track element might not be fetched yet, or it might have been removed from the DOM
  if (track === undefined || !track.isConnected) {
    track = document.getElementById(track_id);
  }
}
